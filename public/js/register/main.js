$(function(){
    $("#form-total").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        autoFocus: true,
        transitionEffectSpeed: 500,
        titleTemplate : '<div class="title">#title#</div>',
        labels: {
            previous : 'Previous',
            next : 'Next Step',
            finish : 'Submit',
            current : ''
        },
        onStepChanging: function (event, currentIndex, newIndex) { 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            switch(currentIndex) {
                case 0:
                    // Validations for Account Information
                    var valid = false;
                    $.ajax({
                        url: $('meta[name="_s1_validator"]').attr('content'),
                        method: 'post',
                        async: false,
                        data: {
                            email: $('#email').val(),
                            password: $('#password_1').val(),
                            password_confirmation: $('#confirm_password_1').val()
                        },
                        success: function(result){
                            if(result.valid == false) {
                                // First Clear Content Errors
                                $('#_s1_Errors').empty();
                                // Populate Errors
                                $.each(result.errors, function(key, value) {
                                    $('#s1Errors').show();
                                    $('#_s1_Errors').append('<li>'+value+'</li>');
                                })
                                valid = false;
                            } else {
                                $('#s1Errors').hide();
                                valid = true;
                            }
                        }
                    });
                    PopulateConfirm()
                    return valid;
                case 1:
                    // Validations for Personal Information
                    var valid = false;
                    $.ajax({
                        url: $('meta[name="_s2_validator"]').attr('content'),
                        method: 'post',
                        async: false,
                        data: {
                            nickname: $('#nickname').val(),
                            firstname: $('#first_name').val(),
                            lastname: $('#last_name').val(),
                            gender: $('form input[type=radio]:checked').val(),
                            birthdate: $('#month') + ' ' + $('#date').val() + ' ' + $('#year').val(),
                            telephone_number: $('#telephone_number').val(),
                            mobile_number: $('#mobile_number').val(),
                            nationality: $('#nationality').val(),
                            country: $('#country').val(),
                            mailing_address: $('#address').val()
                        },
                        success: function(result){
                            if(result.valid == false) {
                                // First Clear Content Errors
                                $('#_s2_Errors').empty();
                                // Populate Errors
                                $.each(result.errors, function(key, value) {
                                    $('#s2Errors').show();
                                    $('#_s2_Errors').append('<li>'+value+'</li>');
                                })
                                valid = false;
                            } else {
                                $('#s2Errors').hide();
                                valid = true;
                            }
                        }
                    });
                    PopulateConfirm()
                    return valid;
                case 2:
                    // Validations for Company Information
                    var valid = false;
                    $.ajax({
                        url: $('meta[name="_s3_validator"]').attr('content'),
                        method: 'post',
                        async: false,
                        data: {
                            business_id: $('#business_id').val(),
                            business_name: $('#business_name').val(),
                            business_address: $('#business_address').val(),
                            business_telephone: $('#business_telephone').val(),
                            business_tags: $('#business_tags').val()
                        },
                        success: function(result){
                            if(result.valid == false) {
                                // First Clear Content Errors
                                $('#_s3_Errors').empty();
                                // Populate Errors
                                $.each(result.errors, function(key, value) {
                                    $('#s3Errors').show();
                                    $('#_s3_Errors').append('<li>'+value+'</li>');
                                })
                                valid = false;
                            } else {
                                if(result.CompanyProfile != null) {
                                    $('#business_name').val(result.CompanyProfile.CompanyName);
                                    $('#business_address').val(result.CompanyProfile.BusinessAddress);
                                    $('#business_telephone').val(result.CompanyProfile.BusinessTelephone);
                                    $('#business_tags').val(result.CompanyProfile.CompanyTags);
                                }
                                $('#s3Errors').hide();
                                valid = true;
                            }
                        }
                    });
                    PopulateConfirm()
                    return valid;
            }
            
            return true;
        },
        onFinishing: function(event, currentIndex) {
            PopulateConfirm();
            return true;
        },
        onFinished: function(event, currentIndex) {
            $('#regForm').submit();
        }
    });
    $("#date").datepicker({
        dateFormat: "MM - DD - yy",
        showOn: "both",
        buttonText : '<i class="zmdi zmdi-chevron-down"></i>',
    });
});

const PopulateConfirm = () => {
    var nickname = $('#nickname').val();
    var fullname = $('#first_name').val() + ' ' + $('#last_name').val() + ($('suffix').val() ? $('suffix').val() : '');
    var email = $('#email').val();
    var TelNumber = $('#telephone_number').val();
    var MobileNumber = $('#mobile_number').val();
    var gender = $('form input[type=radio]:checked').val();
    var birthdate = $('#month').val() + ' ' + $('#date').val() + ', ' + $('#year').val();
    var nationality = $('#nationality').val();
    var country = $('#country').val();
    var address = $('#address').val();
    var BusinessID = $('#business_id').val() == ''? 'Create New Company Profile': 'Joining Company ' + $('#business_id').val();
    var BusinessName = $('#business_name').val();
    var BusinessTel = $('#business_telephone').val();
    var BusinessAddress = $('#business_address').val();
    var BusinessTags = $('#business_tags').val();

    $('#nickname-val').text(nickname)
    $('#fullname-val').text(fullname);
    $('#email-val').text(email);
    $('#telephone-val').text(TelNumber);
    $('#mobile-val').text(MobileNumber);
    $('#gender-val').text(gender);
    $('#birthdate-val').text(birthdate);
    $('#nationality-val').text(nationality);
    $('#country-val').text(country);
    $('#address-val').text(address);
    $('#business-id-val').text(BusinessID);
    $('#business-name-val').text(BusinessName);
    $('#business-tel-val').text(BusinessTel);
    $('#business-address-val').text(BusinessAddress);
    $('#business-tags-val').text(BusinessTags);
}
