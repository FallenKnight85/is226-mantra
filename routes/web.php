<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/



Auth::routes();
//Route::get('/home', 'HomeController@index');
// This line should be show homepage but I have not yet made a home page
// TODO :: change PageController@showLogin to PageController@showHomepage later on
//Route::get('/', 'HomeController@showDashboard');
Route::get('/', 'ProjectsController@getDashboard');
Route::get('/dashboard', 'ProjectsController@getDashboard');
Route::get('logout', 'Auth\LoginController@logout');
Route::get('/my-users', 'HomeController@myUsers');

/* AJAX ROUTES MUST BE DEFINED HERE
 *
 * 
 */
Route::middleware(['ajax'])->group(function () {
    // User Profile
    Route::resource('profile', 'UserProfileController');
    
    // Projects
    Route::resource('project', 'ProjectsController');
    Route::get('/oProjects', 'ProjectsController@getOwnedProjects');
    Route::get('/xProjects', 'ProjectsController@getCurrentProjects');
    Route::get('/aProjects', 'ProjectsController@getAvailableProjects');
    Route::put('/oProjects', 'ProjectsController@create');
    Route::get('/project/{id}/gantt', 'ProjectsController@gantt');

    // User Bids
    Route::resource('bid', 'BidController');
    Route::put('/bid/{id}/approve', 'BidController@approve');
    Route::put('/bid/{id}/status', 'BidController@status');

    // Project Tasks :: Bid
    Route::resource('task', 'TaskController');
    Route::put('/task/{id}/status', 'TaskController@status');

    // Project Sub Tasks :: Bid
    Route::resource('subtask', 'SubtaskController');
    Route::put('/subtask/{id}/status', 'SubtaskController@status');

    //Company Profile
    Route::get('/cProfile', 'CompanyProfileController@getCompanyProfile');
    Route::post('/cProfile', 'CompanyProfileController@post');

    // Registration Validation are all in ajax
    Route::post('/register/step1', 'RegisterValidator@ValidateStep1');
    Route::post('/register/step2', 'RegisterValidator@ValidateStep2');
    Route::post('/register/step3', 'RegisterValidator@ValidateStep3');
});