<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $BidSubTaskID
 * @property string $BidTaskID
 * @property string $CreatedbyProfileID
 * @property string $SubtaskDetails
 * @property string $created_at
 * @property string $updated_at
 */
class BidSubtask extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['BidSubTaskID', 'BidTaskID', 'CreatedbyProfileID', 'SubtaskDetails', 'created_at', 'updated_at'];

}
