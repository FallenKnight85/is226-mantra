<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $ProfileID
 * @property string $UserID
 * @property string $CompanyID
 * @property string $LastName
 * @property string $GivenName
 * @property string $Suffix
 * @property string $NickName
 * @property string $MailingAddress
 * @property string $EmailAddress
 * @property string $BirthDate
 * @property string $Gender
 * @property string $TelephoneNumber
 * @property string $MobileNumber
 * @property string $Nationality
 * @property string $Country
 * @property string $created_at
 * @property string $updated_at
 */
class UserProfile extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['ProfileID', 'UserID', 'CompanyID', 'LastName', 'GivenName', 'Suffix', 'NickName', 'MailingAddress', 'EmailAddress', 'BirthDate', 'Gender', 'TelephoneNumber', 'MobileNumber', 'Nationality', 'Country', 'created_at', 'updated_at'];

}
