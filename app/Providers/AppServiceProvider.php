<?php

namespace App\Providers;

use View;
use App;
use Auth;
use Session;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //fix migration error
        Schema::defaultStringLength(191);

        View::composer('*', function($view) {
            if(\Auth::check()) {
                $Profile = \App\UserProfile::where('UserID', \Auth::user()->UserID)->first();
                $CompanyProfile = \App\CompanyProfile::where('CompanyID', $Profile->CompanyID)->first();

                if ($CompanyProfile->OwnerID == $Profile->ProfileID) {
                    $isManager = 'true';
                }
                else {
                    $isManager = 'false';
                }
                    
                $view->with('nickname', $Profile->NickName);
                $view->with('email', $Profile->EmailAddress);
                $view->with('business_name', $CompanyProfile->CompanyName);
                $view->with('avatar', $Profile->avatar);

                Session::put('FullName', "$Profile->GivenName $Profile->LastName");
                Session::put('CompanyID', $CompanyProfile->CompanyID);
                Session::put('ProfileID', $Profile->ProfileID);
                Session::put('UserID', \Auth::user()->UserID);
                Session::put('isManager', $isManager);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
