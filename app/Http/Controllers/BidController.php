<?php

namespace App\Http\Controllers;

use Session;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ProjectID = $_GET['pid'];
        
        $Project = ProjectsController::getProjectByProjectID($ProjectID);
        $Company = CompanyProfileController::getCompanyProfileByCompanyID($Project->CompanyID);

        if($Project->resource == null){
            $resource = [];
        } else {
            $resource = \json_decode($Project->resource, true);
        }
        
        $data = array_merge(collect($Project)->toArray(), [
            'bid_status' => 'New',
            'project_id' => $Project->ProjectID,
            'company_id' => $Company->CompanyID,
            'project_name' => $Project->ProjectName,
            'project_by' => $Company->CompanyName,
            'project_description' => $Project->ProjectDescription,
            'project_objectives' => $Project->ProjectObjectives,
            'CompanyName' => $Company->CompanyName,
            'resources' => $resource,
            'method'=> 'POST',
            'edit' => true, //true always because this is the create
            ]);

        return view('pages.view_bid', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $newBid = new \App\Bid();
            $newBid->BidID = Uuid::generate()->string;
            $newBid->ForCompanyID = $request->for_company_id;
            $newBid->ByCompanyID = Session::get('CompanyID');
            $newBid->ForProjectID = $request->for_project_id;
            $newBid->CreatebyProfileID = Session::get('ProfileID');
            $newBid->resource = $request->resource;
            $newBid->BidCost = $request->cost;
            $newBid->BidStatus = 'MOpen';
            $newBid->created_at = Carbon::now();
            $newBid->updated_at = Carbon::now();
            $newBid->save();
    
            return \Response::json([
                'status' => 'success',
                'message' => "Successfully created new bid for: $request->project_name of $request->company_name",
                'title' => 'Create Bid'
            ]);
        } 
        catch(\Throwable $th)
        {
            return \Response::json([
                'status' => 'failed',
                'message' => "Failed to create new bid.",
                'title' => 'Create Bid'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentBid = \App\Bid::where('BidID', $id)->first();
        $Project = ProjectsController::getProjectByProjectID($currentBid->ForProjectID);
        $Company = CompanyProfileController::getCompanyProfileByCompanyID($currentBid->ForCompanyID);
        
        if($currentBid->resource == null){
            $resource = [];
        } else {
            $resource = \json_decode($currentBid->resource, true);
        }

        // // Check if edit allowable
        // if($Project->ByCompanyID == Session::get('CompanyID')) {
        //     $edit = true;
        // }
        // else {
        //     $edit = false;
        // }
        
        $data = array_merge(collect($Project)->toArray(), [
            'project_id' => $Project->ProjectID,
            'company_id' => $Company->CompanyID,
            'project_name' => $Project->ProjectName,
            'project_by' => $Company->CompanyName,
            'project_description' => $Project->ProjectDescription,
            'project_objectives' => $Project->ProjectObjectives,
            'CompanyName' => $Company->CompanyName,
            'resources' => $resource,
            'method'=> 'GET',
            'edit' => false, //true always because this is the create
            ]);

        return view('pages.view_bid', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $currentBid = \App\Bid::where('BidID', $id)->first();
        $Project = ProjectsController::getProjectByProjectID($currentBid->ForProjectID);
        $Company = CompanyProfileController::getCompanyProfileByCompanyID($currentBid->ForCompanyID);
        
        if($currentBid->resource == null){
            $resource = [];
        } else {
            $resource = \json_decode($currentBid->resource, true);
        }

        // Check if edit allowable
        if($currentBid->ByCompanyID == Session::get('CompanyID')) {
            $edit = true;
        }
        else {
            $edit = false;
        }
        
        $data = array_merge(collect($Project)->toArray(), [
            'bid_id' => $currentBid->BidID,
            'bid_status' => $currentBid->BidStatus,
            'project_id' => $Project->ProjectID,
            'company_id' => $Company->CompanyID,
            'project_name' => $Project->ProjectName,
            'project_by' => $Company->CompanyName,
            'project_description' => $Project->ProjectDescription,
            'project_objectives' => $Project->ProjectObjectives,
            'CompanyName' => $Company->CompanyName,
            'resources' => $resource,
            'method'=> 'GET',
            'edit' => $edit, //true always because this is the create
            ]);

        return view('pages.view_bid', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $Bid = $currentBid = \App\Bid::where('BidID', $id)->first();
            $Bid->delete();
            return \Response::json([
                'status' => 'success',
                'message' => "Bid has been successfully deleted.",
                'title' => 'Delete Bid'
            ]);
        } catch (\Throwable $th) {
            return \Response::json([
                'status' => 'failed',
                'message' => "Failed to delete bid",
                'title' => 'Delete Bid'
            ]);
        }
    }

    public function approve($id)
    {
        $Bid = \App\Bid::where('BidID', $id)->first();
        $Bid->BidStatus = 'Approved';
        $Bid->save();

        $Project = \App\Project::where('ProjectID', $Bid->ForProjectID)->first();
        $Project->ProjectStatus = 'Approved';
        $Project->BidderCompanyID = $Bid->ByCompanyID;
        $Project->save();

        $Bidder = \App\CompanyProfile::where('CompanyID', $Bid->ByCompanyID)->first();
        
        // Un-approved other Bids
        foreach(\App\Bid::where('BidID', '!=', $id)->where('ForProjectID', $Project->ProjectID)->get() as $unapprovedBid)
        {
            $unapprovedBid->BidStatus = 'Denied';
            $unapprovedBid->save();
        }

        return \Response::json([
            'status' => 'success',
            'message' => "Successfully approved bid from: $Bidder->CompanyName",
            'title' => 'Approve Bid'
        ]);
    }

    public static function hasBid($ProjectID)
    {
        $currentBid = \App\Bid::where('ByCompanyID', Session::get('CompanyID'))
        ->where('ForProjectID', $ProjectID)
        ->get();
        
        if(!$currentBid->isEmpty()) {
            return ([
                'hasbid' => 'true',
                'bid_id' => $currentBid->first()->BidID
            ]);
        } else {
            return ([
                'hasbid' => 'false',
                'bid_id' => ''
            ]);
        }
    }

    public function status(Request $request, $id)
    {
        try {
            $Bid = \App\Bid::where('BidID', $id)->first();
            $Bid->BidStatus = $request->status;
            $Bid->save();

            return \Response::json([
                'status' => 'success',
                'message' => "Successfully marked $request->status",
                'title' => 'Change Status'
            ]);
        } 
        catch(\Throwable $th)
        {
            return \Response::json([
                'status' => 'failed',
                'message' => "Failed to marked $request->status",
                'title' => 'Change Status'
            ]);
        }
    }
}
