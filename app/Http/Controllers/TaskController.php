<?php

namespace App\Http\Controllers;

use Session;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ProjectID = $_GET['pid'];
        $Project = \App\Project::where('ProjectID', $ProjectID)->first();
        $CompanyName = \App\CompanyProfile::where('CompanyID', $Project->CompanyID)->first()->CompanyName;

        $data = array_merge(collect($Project)->toArray(), [
            'TaskID' => '',
            'TaskName' => '',
            'TaskDescription' => '',
            'CompanyName' => $CompanyName,
            'uri' => url('/task'),
            'method' => 'POST',
            'edit' => true,
        ]);

        return view('pages.view_task', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $Task = new \App\Task();
            $Task->TaskID = Uuid::generate()->string;
            $Task->ProjectID = $request->project_id;
            $Task->CreatedbyProfileID = Session::get('ProfileID');
            $Task->TaskName = $request->task_name;
            $Task->TaskDescription = $request->task_description;
            $Task->TaskStatus = 'MOpen';
            $Task->created_at = Carbon::now();
            $Task->updated_at = Carbon::now();
            $Task->save();
    
            return \Response::json([
                'status' => 'success',
                'message' => "Successfully created new task.",
                'title' => 'Create Task'
            ]);
        } 
        catch(\Throwable $th)
        {
            return \Response::json([
                'status' => 'failed',
                'message' => "Failed to create new Task.",
                'title' => 'Create Task'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $Project = \App\Project::where('ProjectID', $id)->first();
        $Company = CompanyProfileController::getCompanyProfileByCompanyID($Project->CompanyID)->first();
        $Tasks = \App\Task::where('ProjectID', $Project->ProjectID)
        ->get()
        ->map(function($task, $key) {
            $subtasks = \App\Subtask::where('TaskID', $task->TaskID)
            ->get()
            ->map(function($subtask, $k) {
                $subtask_edit_uri = url('/subtask')."/$subtask->SubTaskID/edit";
                $subtask_delete_uri = url('/subtask')."/$subtask->SubTaskID";
                $subtask->subtask_edit_uri = $subtask_edit_uri; 
                $subtask->subtask_delete_uri = $subtask_delete_uri;
                return $subtask;
            });

            $task_edit_uri = url('/task')."/$task->TaskID/edit";
            $task_delete_uri = url('/task')."/$task->TaskID";
            $subtask_create_uri = url('/subtask/create').'/?'.http_build_query(['tid'=>$task->TaskID]);

            $task->task_edit_uri = $task_edit_uri; 
            $task->task_delete_uri = $task_delete_uri;
            $task->index = $key + 1;
            $task->subtasks = $subtasks;
            $task->subtask_create_uri = $subtask_create_uri;

            return $task;
        });

        $task_create_uri = url('/task/create').'/?'.http_build_query(['pid'=>$Project->ProjectID]);

        if($Project->CompanyID == Session::get('CompanyID')) {
            $owned = 'true';
        } else {
            $owned = 'false';
        }

        return view('pages.compose_tasks', [
            'project_id' => $Project->ProjectID,
            'project_name' => $Project->ProjectName,
            'project_by' => $Company->CompanyName,
            'project_description' => $Project->ProjectDescription,
            'project_objectives' => $Project->ProjectObjectives,
            'task_create_uri' => $task_create_uri,
            'tasks' => collect($Tasks)->toArray(),
            'owned' => $owned,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Task = \App\Task::where('TaskID', $id)->first();
        $Project = \App\Project::where('ProjectID', $Task->ProjectID)->first();
        $CompanyName = \App\CompanyProfile::where('CompanyID', $Project->CompanyID)->first()->CompanyName;

        $data = array_merge(
            collect($Task)->toArray(),
            collect($Project)->toArray(),
            [
                'CompanyName' => $CompanyName,
                'uri' => url('/task')."/$id",
                'method' => 'PUT',
                'edit' => true,
            ]
        );

        return view('pages.view_task', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $Task = \App\Task::where('TaskID', $id)->first();
            $Task->TaskName = $request->task_name;
            $Task->TaskDescription = $request->task_description;
            $Task->updated_at = Carbon::now();
            $Task->save();
    
            return \Response::json([
                'status' => 'success',
                'message' => "Successfully updated task.",
                'title' => 'Edit Task'
            ]);
        } 
        catch(\Throwable $th)
        {
            return \Response::json([
                'status' => 'failed',
                'message' => "Failed to update Task.",
                'title' => 'Edit Task'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Task = \App\Task::where('TaskID', $id);

            // delete subtasks
            \App\Subtask::where('TaskID', $id)->delete();

            // delete task
            $Task->delete();

            return \Response::json([
                'status' => 'success',
                'message' => "Successfully deleted task.",
                'title' => 'Delete Task'
            ]);
        } 
        catch(\Throwable $th)
        {
            return \Response::json([
                'status' => 'failed',
                'message' => "Failed to delete Task.",
                'title' => 'Delete Task'
            ]);
        }
    }

    public function status(Request $request, $id)
    {
        try {
            $Task = \App\Task::where('TaskID', $id)->first();
            $Task->TaskStatus = $request->status;
            $Task->save();

            return \Response::json([
                'status' => 'success',
                'message' => "Successfully marked $request->status",
                'title' => 'Change Status'
            ]);
        } 
        catch(\Throwable $th)
        {
            return \Response::json([
                'status' => 'failed',
                'message' => "Failed to marked $request->status",
                'title' => 'Change Status'
            ]);
        }
    }
}
