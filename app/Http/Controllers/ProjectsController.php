<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Swatkins\LaravelGantt\Gantt;
use Illuminate\Support\Collection;

class ProjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $ProjectID = $_GET['pid'];
        $bids = \App\Bid::where('ForProjectID', $ProjectID)
        ->get()
        ->map(function($bid, $key) {
            if($bid->resource == null){
                $resource = [];
            } else {
                $resource = \json_decode($bid->resource, true);
            }

            $total_cost = 0;

            foreach ($resource as $res) {
                $total_cost += $res['cost'] * $res['quantity'];
            }

            $data = array_merge(collect($bid)->toArray(), [
                'company_name' => \App\CompanyProfile::where('CompanyID', $bid->ByCompanyID)->first()->CompanyName,
                'resources' => $resource,
                'resources_count' => count($resource),
                'index' => $key + 1,
                'total_cost' => $total_cost,
                ]);
                
            return $data;
        });;

        return view('pages.view_vendor_bids', [
            'bids' => $bids,
        ]);
    }

    public function show($id)
    {
        $Project = \App\Project::where('ProjectID', $id)->first();
        $Company = \App\CompanyProfile::where('CompanyID', $Project->CompanyID)->first();

        if($Project->resource == null){
            $resource = [];
        } else {
            $resource = \json_decode($Project->resource, true);
        }

        // // Check if edit allowable
        // if($Project->CompanyID == Session::get('CompanyID')) {
        //     $edit = true;
        // }
        // else {
        //     $edit = false;
        // }
        
        $data = array_merge(collect($Project)->toArray(), [
            'CompanyName' => $Company->CompanyName,
            'resources' => $resource,
            'edit' => false,
            ]);

        return view('pages.view_project', $data);
    }

    public function update(Request $data, $id)
    {
        try {
            $Project = \App\Project::where('ProjectID', $id)->first();
            $Project->ProjectName = $data->project_name;
            $Project->ProjectDescription = $data->project_description;
            $Project->ProjectObjectives = $data->project_objectives;
            $Project->resource = $data->project_resources;
            $Project->updated_at = Carbon::now();
            $Project->save();
    
            return \Response::json([
                'status' => 'success',
                'message' => "Successfully edited project: $Project->ProjectID",
                'title' => 'Edit Project'
            ]);
        } catch (\Throwable $th) {
            return \Response::json([
                'status' => 'error',
                'message' => "Failed to edit project: $Project->ProjectID",
                'title' => 'Edit Project'
            ]);
        }
    }

    public function create(Request $data) 
    {
        $newProject = new \App\Project();
        $newProject->ProjectID = Uuid::generate()->string;
        $newProject->CompanyID = Session::get('CompanyID');
        $newProject->CreatebyProfileID = Session::get('ProfileID');
        $newProject->ProjectName = $data->project_name;
        $newProject->ProjectDescription = $data->project_description;
        $newProject->ProjectObjectives = $data->project_objectives;
        $newProject->resource = $data->project_resources;
        $newProject->ProjectStatus = 'Open';
        $newProject->created_at = Carbon::now();
        $newProject->updated_at = Carbon::now();
        $newProject->save();

        return \Response::json([
            'status' => 'success',
            'message' => "Successfully created new project: $newProject->ProjectName",
            'title' => 'Create Project'
        ]);
    }

    public function edit($id)
    {
        $Project = \App\Project::where('ProjectID', $id)->first();
        $Company = \App\CompanyProfile::where('CompanyID', $Project->CompanyID)->first();

        if($Project->resource == null){
            $resource = [];
        } else {
            $resource = \json_decode($Project->resource, true);
        }

        // Check if edit allowable
        if($Project->CompanyID == Session::get('CompanyID') && $Project->ProjectStatus == 'Open' && ($Project->CreatebyProfileID == Auth::user()->ProfileID || Session::get('isManager') == 'true')) {
            $edit = true;
        }
        else {
            $edit = false;
        }
        
        $data = array_merge(collect($Project)->toArray(), [
            'CompanyName' => $Company->CompanyName,
            'resources' => $resource,
            'edit' => $edit,
            ]);

        return view('pages.view_project', $data);
    }

    public function destroy($id) 
    {
        $Project = \App\Project::where('ProjectID', $id)->first();
        $ProjectName = $Project->ProjectName;
        $Project->delete();
        return \Response::json([
            'status' => 'success',
            'message' => "Project: $ProjectName has been successfully deleted.",
            'title' => 'Delete Project'
        ]);
    }

    //
    public function getOwnedProjects() 
    {
        $ownedProjects = \App\Project::where('CompanyID', Session::get('CompanyID'))
        ->where('ProjectStatus', 'Open')
        ->get()
        ->map(function($project) {
            $bids = \App\Bid::where('ForProjectID', $project->ProjectID)->get();
            $project->bid_count = count($bids);
            $project->query = http_build_query(['pid'=>$project->ProjectID]);
            return $project;
        });

        return view('pages.oprojects', [
            'projects' => $ownedProjects
        ]);
    }

    public function getCurrentProjects() 
    {
        return view('pages.xprojects', [
            'approvedOwnedProjects' => $this->getApprovedOwnedProjects(),
            'approvedProjects' => $this->getApprovedProjects(),
        ]);
    }

    public function getAvailableProjects() 
    {
        $availProjects = \DB::table('projects')
        ->leftJoin('company_profiles', 'projects.CompanyID', '=', 'company_profiles.CompanyID')
        ->where('projects.ProjectStatus', 'Open')
        ->where('projects.CompanyID', '!=', Session::get('CompanyID'))
        ->whereNull('BidderCompanyID')
        ->orderBy('projects.created_at', 'DESC')
        ->get()
        ->map(function($project) {
            $bid_info = BidController::hasBid($project->ProjectID);
            $project->me_hasbid = $bid_info['hasbid'];
            $project->me_BidID = $bid_info['bid_id'];
            $project->query = http_build_query(['pid'=>$project->ProjectID]);

            return $project;
        });

        return view('pages.aprojects', [
            'projects' => $availProjects
        ]);
    }

    public function getDashboard() 
    {
        // $data = array_merge($this->getApprovedOwnedProjects(), $this->getApprovedProjects());
        $collection = new Collection(
            collect($this->getApprovedOwnedProjects())->toArray(), collect($this->getApprovedProjects())->toArray()
        );

        $completedCollection = new Collection(
            collect($this->getCompletedOwnedProjects())->toArray(), collect($this->getCompletedProjects())->toArray()
        );
        
        $data = $collection->sortBy(function($p)
        {
            // print_r($p[0]['project_progress']['project_progress']);
            // die();
            try {
                return $p[0]['project_progress']['project_progress'];
            } catch (\Throwable $th) {
                return $p;
            }
        })->take(10);

        $collectedData = collect($data)->toArray();
        // foreach($data as $project)
        // {
        //     print_r($project[0]['company_name']);
        //     print_r($project[0]['project_progress']['project_progress']);
        //     die();
        // }

        //dd($collectedData);

        //dd($collectedData[0]['project_progress']->project_progress);

        return view('dashboard', [
            'projects' => $collectedData,
            'countApproved' => count($collection),
            'countCompleted' => count($completedCollection),
        ]);
    }

    public function gantt($id)
    {
        $ProjectName = \App\Project::where('ProjectID', $id)->first()->ProjectName;
        $TaskIDs = \App\Task::where('ProjectID', $id)->select('TaskID')->get();
        /**
         * Get your model items however you deem necessary
         */
        $select = 'SubtaskDetails as label, DATE_FORMAT(IFNULL(DateApproved, created_at), \'%Y-%m-%d\') as start, DATE_FORMAT(IFNULL(TargetDate, now()), \'%Y-%m-%d\') as end';
        $projects = \App\Subtask::select(\Illuminate\Support\Facades\DB::raw($select))
                        ->whereIn('TaskID', collect($TaskIDs)->toArray())
                        ->orderBy('start', 'asc')
                        ->orderBy('end', 'asc')
                        ->get();
        
        if(count($projects) > 0) {
            $gantt = new \Swatkins\LaravelGantt\Gantt($projects->toArray(), array(
                'title'      => $ProjectName,
                'cellwidth'  => 25,
                'cellheight' => 35
            ));
    
            return view('pages.view_gantt', [
                'gantt' => $gantt
            ]);
        } else {
            $data = "<div class=\"text-center text-danger\">No data available...</div>";
            return view('pages.view_gantt', [
                'gantt' => $data
            ]);
        }
        /**
         *  You'll pass data as an array in this format:
         *  [
         *    [ 
         *      'label' => 'The item title',
         *      'start' => '2016-10-08',
         *      'end'   => '2016-10-14'
         *    ]
         *  ]
         */
    }

    /* PRIVATE FUNCTIONS
     *
     * 
     */

    private function getApprovedOwnedProjects()
    {
        $approvedOwnedProjects = \App\Project::where('CompanyID', Session::get('CompanyID'))
        ->where('ProjectStatus', 'Approved')
        ->get()
        ->map(function($project) {
            $bid = \App\Bid::where('ForProjectID', $project->ProjectID)
            ->where('BidStatus', 'Approved')
            ->first();
            $bidder = \App\CompanyProfile::where('CompanyID', $bid->ForCompanyID)->first();
            $project->bid_status = $bid->BidStatus;
            $project->company_name = $bidder->CompanyName;
            $project->gantt_uri = url('/project')."/$project->ProjectID/gantt";
            $project->project_progress = ProjectsController::getProjectProgress($project->ProjectID);
            return $project;
        });
        return $approvedOwnedProjects;
    }

    private function getCompletedOwnedProjects()
    {
        $approvedOwnedProjects = \App\Project::where('CompanyID', Session::get('CompanyID'))
        ->where('ProjectStatus', 'Completed')
        ->get()
        ->map(function($project) {
            $bid = \App\Bid::where('ForProjectID', $project->ProjectID)
            ->where('BidStatus', 'Approved')
            ->first();
            $bidder = \App\CompanyProfile::where('CompanyID', $bid->ForCompanyID)->first();
            $project->bid_status = $bid->BidStatus;
            $project->company_name = $bidder->CompanyName;
            $project->gantt_uri = url('/project')."/$project->ProjectID/gantt";
            $project->project_progress = ProjectsController::getProjectProgress($project->ProjectID);
            return $project;
        });
        return $approvedOwnedProjects;
    }

    private function getApprovedProjects()
    {
        $approvedProjects = \App\Project::where('BidderCompanyID', Session::get('CompanyID'))
        ->where('ProjectStatus', 'Approved')
        ->get()
        ->map(function($project) {
            $bid = \App\Bid::where('ForProjectID', $project->ProjectID)
            ->where('BidStatus', 'Approved')
            ->first();
            $owner = \App\CompanyProfile::where('CompanyID', $bid->ForCompanyID)->first();
            $project->bid_status = $bid->BidStatus;
            $project->company_name = $owner->CompanyName;
            $project->gantt_uri = url('/project')."/$project->ProjectID/gantt";
            $project->project_progress = ProjectsController::getProjectProgress($project->ProjectID);
            return $project;
        });
        return $approvedProjects;
    }

    private function getCompletedProjects()
    {
        $approvedProjects = \App\Project::where('BidderCompanyID', Session::get('CompanyID'))
        ->where('ProjectStatus', 'Completed')
        ->get()
        ->map(function($project) {
            $bid = \App\Bid::where('ForProjectID', $project->ProjectID)
            ->where('BidStatus', 'Approved')
            ->first();
            $owner = \App\CompanyProfile::where('CompanyID', $bid->ForCompanyID)->first();
            $project->bid_status = $bid->BidStatus;
            $project->company_name = $owner->CompanyName;
            $project->gantt_uri = url('/project')."/$project->ProjectID/gantt";
            $project->project_progress = ProjectsController::getProjectProgress($project->ProjectID);
            return $project;
        });
        return $approvedProjects;
    }


    /* STATIC FUNCTIONS
     *
     * 
     */

    public static function getProjectByProjectID($ProjectID)
    {
        return \App\Project::where('ProjectID', $ProjectID)->first();
    }

    public static function getProjectProgress($id)
    {
        $Project = \App\Project::where('ProjectID', $id)
        ->get()
        ->map(function($p, $k) {
            $Tasks = \App\Task::where('ProjectID', $p->ProjectID)
            ->get()
            ->map(function($task, $key) {
                $subtasks = \App\Subtask::where('TaskID', $task->TaskID)->get();
                
                $task->count_subtasks = $subtasks->count();
                $task->count_subtasks_open = $subtasks->filter(function ($subtask) { return ($subtask->SubTaskStatus == 'Open'); })->count();
                $task->count_subtasks_submit = $subtasks->filter(function ($subtask) { return ($subtask->SubTaskStatus == 'Submit'); })->count();
                $task->count_subtasks_approved = $subtasks->filter(function ($subtask) { return ($subtask->SubTaskStatus == 'Approved'); })->count();
                $task->count_subtasks_done = $subtasks->filter(function ($subtask) { return ($subtask->SubTaskStatus == 'Done'); })->count();
                $task->count_subtasks_closed = $subtasks->filter(function ($subtask) { return ($subtask->SubTaskStatus == 'Closed'); })->count();

                $task->task_progress = ($task->count_subtasks == 0) ? 0 : ($task->count_subtasks_closed / $task->count_subtasks) * 100;
                $task->task_status = ($task->progress == 100) ? 'Completed' : 'On-Going';

                return $task;
            });

            $p->count_tasks = 0;
            $p->count_tasks_completed = 0;
            $p->count_subtasks_total = 0;
            $p->count_subtasks_total_open = 0;
            $p->count_subtasks_total_submit = 0;
            $p->count_subtasks_total_approved = 0;
            $p->count_subtasks_total_done = 0;
            $p->count_subtasks_total_closed = 0;
            foreach($Tasks as $task)
            {
                $p->count_tasks += 1;
                if($task->task_status == 'Completed') $p->count_tasks_completed += 1;
                $p->count_subtasks_total += $task->count_subtasks;
                $p->count_subtasks_total_open += $task->count_subtasks_open;
                $p->count_subtasks_total_submit += $task->count_subtasks_submit;
                $p->count_subtasks_total_approved += $task->count_subtasks_approved;
                $p->count_subtasks_total_done += $task->count_subtasks_done;
                $p->count_subtasks_total_closed += $task->count_subtasks_closed;
            }

            $p->project_progress = ($p->count_tasks == 0) ? 0 : ($p->count_tasks_completed / $p->count_tasks) * 100;
            $p->project_status = ($p->progress == 100) ? 'Completed' : 'On-Going';

            return $p;
        });

        return $Project->first();
    }
}
