<?php

namespace App\Http\Controllers;

use Session;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SubtaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $TaskID = $_GET['tid'];
        $Task = \App\Task::where('TaskID', $TaskID)->first();
        $Project = \App\Project::where('ProjectID', $Task->ProjectID)->first();
        $CompanyName = \App\CompanyProfile::where('CompanyID', $Project->CompanyID)->first()->CompanyName;

        $data = array_merge(
            collect($Task)->toArray(),
            collect($Project)->toArray(), 
            [
            'SubtaskDetails' => '',
            'TargetDate' => '',
            'CompanyName' => $CompanyName,
            'uri' => url('/subtask'),
            'method' => 'POST',
            'edit' => true,
            ]
        );

        return view('pages.view_subtask', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $Subtask = new \App\Subtask();
            $Subtask->SubTaskID = Uuid::generate()->string;
            $Subtask->TaskID = $request->task_id;
            $Subtask->CreatedbyProfileID = Session::get('ProfileID');
            $Subtask->SubtaskDetails = $request->subtask_details;
            $Subtask->TargetDate = $request->target_date;
            $Subtask->SubTaskStatus = 'MOpen';
            $Subtask->created_at = Carbon::now();
            $Subtask->updated_at = Carbon::now();
            $Subtask->save();
    
            return \Response::json([
                'status' => 'success',
                'message' => "Successfully created new Subtask.",
                'title' => 'Create Subtask'
            ]);
        } 
        catch(\Throwable $th)
        {
            return \Response::json([
                'status' => 'failed',
                'message' => "Failed to create new Subtask.",
                'title' => 'Create Subtask'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $SubTask = \App\Subtask::where('SubTaskID', $id)->first();
        $Task = \App\Task::where('TaskID', $SubTask->TaskID)->first();
        $Project = \App\Project::where('ProjectID', $Task->ProjectID)->first();
        $CompanyName = \App\CompanyProfile::where('CompanyID', $Project->CompanyID)->first()->CompanyName;

        $data = array_merge(
            collect($SubTask)->toArray(),
            collect($Task)->toArray(),
            collect($Project)->toArray(), 
            [
            'CompanyName' => $CompanyName,
            'uri' => url('/subtask')."/$id",
            'method' => 'PUT',
            'edit' => true,
            ]
        );

        return view('pages.view_subtask', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $Subtask = \App\Subtask::where('SubTaskID', $id)->first();
            $Subtask->SubtaskDetails = $request->subtask_details;
            $Subtask->TargetDate = $request->target_date;
            $Subtask->updated_at = Carbon::now();
            $Subtask->save();
    
            return \Response::json([
                'status' => 'success',
                'message' => "Successfully updated task.",
                'title' => 'Edit Task'
            ]);
        } 
        catch(\Throwable $th)
        {
            return \Response::json([
                'status' => 'failed',
                'message' => "Failed to update Task.",
                'title' => 'Edit Task'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Subtask = \App\Subtask::where('SubTaskID', $id)->first();
            $Subtask->delete();

            return \Response::json([
                'status' => 'success',
                'message' => "Successfully deleted subtask.",
                'title' => 'Delete Subtask'
            ]);
        } 
        catch(\Throwable $th)
        {
            return \Response::json([
                'status' => 'failed',
                'message' => "Failed to delete subtask.",
                'title' => 'Delete Subtask'
            ]);
        }
    }

    public function status(Request $request, $id)
    {
        try {
            $Subtask = \App\Subtask::where('SubTaskID', $id)->first();
            $Subtask->SubTaskStatus = $request->status;

            switch ($request->status) {
                case 'Approve':
                    $Subtask->DateApproved = Carbon::now();
                    break;

                case 'Close':
                    $Subtask->DateCompleted = Carbon::now();
                    break;
                
                default:
                    break;
            }

            $Subtask->save();

            return \Response::json([
                'status' => 'success',
                'message' => "Successfully marked $request->status",
                'title' => 'Change Status'
            ]);
        } 
        catch(\Throwable $th)
        {
            return \Response::json([
                'status' => 'failed',
                'message' => "Failed to marked $request->status",
                'title' => 'Change Status'
            ]);
        }
    }
}
