<?php

namespace App\Http\Controllers;

use Session;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('pages.uprofile', \App\UserProfile::where('ProfileID', $id)->first());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try 
        {
            $Profile = \App\UserProfile::where('ProfileID', Session::get('ProfileID'))->first();
            $Profile->LastName = $request->last_name;
            $Profile->GivenName = $request->given_name;
            $Profile->Suffix = $request->suffix;
            $Profile->NickName = $request->nickname;
            $Profile->MailingAddress = $request->mailing_address;
            $Profile->Gender = $request->gender;
            $Profile->TelephoneNumber = $request->telephone_number;
            $Profile->MobileNumber = $request->mobile_number;
            $Profile->Nationality = $request->nationality;
            $Profile->Country = $request->country;
            $Profile->Birthdate = Carbon::parse($request->birth_date);
            $Profile->updated_at = Carbon::now();
            $Profile->save();

            return \Response::json([
                'status' => 'success',
                'message' => 'Successfully updated user profile.',
                'title' => 'User Profile Update'
            ]);
        }
        catch(Exception $e)
        {
            return \Response::json([
                'status' => 'error',
                'message' => $e->message,
                'title' => 'User Profile Update'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
