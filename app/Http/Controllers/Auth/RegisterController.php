<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        extract($data);

        // Step 1 Create User
        $User = new \App\User();
        $User->UserID = Uuid::generate()->string;
        $User->Username = $email;
        $User->password = Hash::make($password_1);
        $User->email = $email;
        $User->created_at = Carbon::now();
        $User->updated_at = Carbon::now();
        
        // Step 2 Create the User Profile
        $UserProfile = new \App\UserProfile();
        $UserProfile->ProfileID = Uuid::generate()->string;
        $UserProfile->UserID = $User->UserID;
        $UserProfile->CompanyID = ($business_id=='' ? 0 : $business_id);
        $UserProfile->LastName = $last_name;
        $UserProfile->GivenName = $first_name;
        $UserProfile->Suffix = $suffix;
        $UserProfile->NickName = $nickname;
        $UserProfile->MailingAddress = $address;
        $UserProfile->EmailAddress = $User->email;
        $birthdate = $month.' '.$date.', '.$year;
        $UserProfile->Birthdate = Carbon::parse($birthdate);
        $UserProfile->Gender = $gender;
        $UserProfile->TelephoneNumber = $telephone_number;
        $UserProfile->MobileNumber = $mobile_number;
        $UserProfile->Nationality = $nationality;
        $UserProfile->Country = $country;
        $UserProfile->created_at = Carbon::now();
        $UserProfile->updated_at = Carbon::now();

        // Step 3 Create Company Profile if New
        if($business_id == '') {
            $CompanyProfile = new \App\CompanyProfile();
            $CompanyProfile->CompanyID = Uuid::generate()->string;
            $CompanyProfile->OwnerID = $UserProfile->ProfileID;
            $CompanyProfile->CompanyName = $business_name;
            $CompanyProfile->BusinessAddress = $business_address;
            $CompanyProfile->BusinessTelephone = $business_telephone;
            $CompanyProfile->CompanyTags = $business_tags;
            $CompanyProfile->created_at = Carbon::now();
            $CompanyProfile->updated_at = Carbon::now();
            $CompanyProfile->save();

            $UserProfile->CompanyID = $CompanyProfile->CompanyID;
        }
        
        $User->save();
        $UserProfile->save();
        return $User;
    }
}
