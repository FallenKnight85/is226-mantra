<?php

namespace App\Http\Controllers;

use Session;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CompanyProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function getCompanyProfile() {
        $Profile = \App\CompanyProfile::where('CompanyID', Session::get('CompanyID'))->first();
        $UProfiles = \App\UserProfile::where('CompanyID', Session::get('CompanyID'))
        ->get()
        ->map(function($p) {
            $c = \App\CompanyProfile::where('CompanyID', Session::get('CompanyID'))->first();
            $p->o = false;
            if($c->OwnerID == $p->ProfileID) 
            {
                $p->o = true;
            } else {
                $p->o = false;
            }
            return $p;
        });
        return view('pages.cprofile', [
            'owner' => Session::get('isManager'),
            'business_id' => $Profile->CompanyID,
            'business_name' => $Profile->CompanyName,
            'business_address' => $Profile->BusinessAddress,
            'business_telephone' => $Profile->BusinessTelephone,
            'business_tags' => $Profile->CompanyTags,
            'profiles' => $UProfiles,
        ]);
    }

    public function post(Request $data) {
        $Profile = \App\CompanyProfile::where('CompanyID', Session::get('CompanyID'))->first();
        $Profile->CompanyName = $data->business_name;
        $Profile->BusinessAddress = $data->business_address;
        $Profile->BusinessTelephone = $data->business_telephone;
        $Profile->CompanyTags = $data->business_tags;
        $Profile->save();
        // toastr()->success('Data has been saved successfully!');
        // return back();
        return \Response::json([
            'status' => 'success',
            'message' => 'Business Profile has been updated.',
            'title' => 'Company Profile Update',
        ]);
    }

    public static function getCompanyProfileByCompanyID($CompanyID)
    {
        return \App\CompanyProfile::where('CompanyID', $CompanyID)->first();
    }
}
