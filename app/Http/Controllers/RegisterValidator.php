<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use App\Http\Controllers\Controller;

class RegisterValidator extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ValidateStep1(Request $data)
    {
        $validator = \Validator::make($data->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed'
        ]);

        if ($validator->fails())
        {
            return response()->json(['valid'=>false,'errors'=>$validator->errors()->all()]);
        }

        return response()->json(['valid'=>true]);
    }

    public function ValidateStep2(Request $data)
    {
        $validator = \Validator::make($data->all(), [
            'nickname' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'gender' => 'required',
            'birthdate' => 'required',
            'telephone_number' => 'required',
            'mobile_number' => 'required',
            'nationality' => 'required',
            'country' => 'required',
            'mailing_address' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['valid'=>false,'errors'=>$validator->errors()->all()]);
        }

        return response()->json(['valid'=>true]);
    }

    public function ValidateStep3(Request $data)
    {
        if($data->business_id != '') {
            $CompanyProfile = \App\CompanyProfile::where('CompanyID', $data->business_id)->first();
            if($CompanyProfile != null) {
                return response()->json(['valid'=>true, 'CompanyProfile'=>$CompanyProfile]);
            } else {
                return response()->json(['valid'=>false,'errors'=>['Company ID does not Exist.']]);
            }
        } else {
            $validator = \Validator::make($data->all(), [
                'business_name' => 'required|string|unique:company_profiles,CompanyName',
                'business_address' => 'required|string',
                'business_telephone' => 'required|string',
                'business_tags' => 'required|string',
            ]);
    
            if ($validator->fails())
            {
                return response()->json(['valid'=>false,'errors'=>$validator->errors()->all()]);
            }
        }

        return response()->json(['valid'=>true]);
    }
}