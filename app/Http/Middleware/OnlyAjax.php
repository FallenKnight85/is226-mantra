<?php

namespace App\Http\Middleware;

use Closure;

class OnlyAjax
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $storedRequest = $request;

        if ( !$request->ajax()) {
            abort(403);
        }
            //return redirect('/');
            //return response('Forbidden.', 403);

        return $next($storedRequest);
    }
}
