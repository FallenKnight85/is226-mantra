<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $CompanyID
 * @property string $OwnerID
 * @property string $CompanyName
 * @property string $BusinessAddress
 * @property string $BusinessTelephone
 * @property string $CompanyTags
 * @property string $created_at
 * @property string $updated_at
 */
class CompanyProfile extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['CompanyID', 'OwnerID', 'CompanyName', 'BusinessAddress', 'BusinessTelephone', 'CompanyTags', 'created_at', 'updated_at'];

}
