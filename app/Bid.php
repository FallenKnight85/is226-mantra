<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $BidID
 * @property string $ForCompanyID
 * @property string $ByCompanyID
 * @property string $ForProjectID
 * @property string $CreatebyProfileID
 * @property string $BidTitle
 * @property string $BidDescription
 * @property float $BidCost
 * @property string $BidStatus
 * @property string $created_at
 * @property string $updated_at
 */
class Bid extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['BidID', 'ForCompanyID', 'ByCompanyID', 'ForProjectID', 'CreatebyProfileID', 'BidTitle', 'BidDescription', 'BidCost', 'BidStatus', 'created_at', 'updated_at'];

}
