<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $ProjectID
 * @property string $CompanyID
 * @property string $CreatebyProfileID
 * @property string $ProjectName
 * @property string $ProjectDescription
 * @property string $ProjectObjectives
 * @property string $ProjectStatus
 * @property string $created_at
 * @property string $updated_at
 */
class Project extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['ProjectID', 'CompanyID', 'CreatebyProfileID', 'ProjectName', 'ProjectDescription', 'ProjectObjectives', 'ProjectStatus', 'created_at', 'updated_at'];

}
