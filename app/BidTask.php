<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $BidTaskID
 * @property string $BidID
 * @property string $CreatedbyProfileID
 * @property string $TaskName
 * @property string $TaskDescription
 * @property string $created_at
 * @property string $updated_at
 */
class BidTask extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['BidTaskID', 'BidID', 'CreatedbyProfileID', 'TaskName', 'TaskDescription', 'created_at', 'updated_at'];

}
