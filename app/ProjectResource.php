<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectResource extends Model
{
    //
    public $name;
    public $addedby;
    public $quantity;
    public $cost;
}
