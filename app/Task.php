<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $TaskID
 * @property string $ProjectID
 * @property string $CreatedbyProfileID
 * @property string $TaskName
 * @property string $TaskDescription
 * @property string $TaskStatus
 * @property string $created_at
 * @property string $updated_at
 */
class Task extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['TaskID', 'ProjectID', 'CreatedbyProfileID', 'TaskName', 'TaskDescription', 'TaskStatus', 'created_at', 'updated_at'];

}
