<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $SubTaskID
 * @property string $TaskID
 * @property string $CreatedbyProfileID
 * @property string $SubtaskDetails
 * @property string $SubTaskStatus
 * @property string $created_at
 * @property string $updated_at
 */
class Subtask extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['SubTaskID', 'TaskID', 'CreatedbyProfileID', 'SubtaskDetails', 'SubTaskStatus', 'created_at', 'updated_at'];

}
