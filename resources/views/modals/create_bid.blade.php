<!-- modal large -->
<div class="modal fade" id="create_bid" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Post a New Bid</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-header">
                        Create <strong>Bid</strong>
                    </div>
                    <div class="card-body card-block">
                        <form action="" method="post" class="">
                            <div class="form-group">
                                <label for="bid_title" class=" form-control-label"><strong>Bid Title</strong></label>
                                <input type="text" id="bid_title" name="bid_title" placeholder="Enter Project Name.." class="form-control">
                                <span class="help-block">Enter a title for this bid.</span>
                            </div>
                            <div class="form-group">
                                <label for="bid_description" class=" form-control-label"><strong>Bid Description</strong></label>
                                <textarea rows=5 id="bid_description" name="bid_description" placeholder="Enter Project Description.." class="form-control"></textarea>
                                <span class="help-block">Describe this bid.</span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="for_company_id" id="for_company_id">
                <input type="hidden" name="for_project_id" id="for_project_id">
                <input type="hidden" name="project_name" id="project_name">
                <input type="hidden" name="company_name" id="company_name">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" name="create_bid_submit" id="create_bid_submit" class="btn btn-primary">Confirm</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal large -->

<!-- script -->
<script>
    $(document).ready(function(){
        $('#create_bid_submit').click(function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            
            $.ajax({
                url: '{{ url('/bid') }}',
                method: 'put',
                data: {
                    bid_title: $('#bid_title').val(),
                    bid_description: $('#bid_description').val(),
                    for_company_id: $('#for_company_id').val(),
                    for_project_id: $('#for_project_id').val(),
                    project_name: $('#project_name').val(),
                    company_name: $('#company_name').val()
                },
                success: function(result){
                    if(result.status == 'success') {
                        toastr.success(result.message, result.title);
                    } else {
                        toastr.error(result.message, result.title);
                    }
                    $('#create_bid').modal('hide');
                    $('.page-container').load('{{ url('/bid') }}\/' + result.bid_id);
                }
            });
        });
    });
</script>