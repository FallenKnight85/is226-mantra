<!-- modal large -->
<div class="modal fade" id="create_project" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Post a New Project</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                    <div class="card">
                        <div class="card-header">
                            New <strong>Unamed Project</strong>
                        </div>
                        <div class="card-body card-block">
                        <form id="create_project_form">
                            <div class="form-group">
                                <label for="project_name" class=" form-control-label"><strong>Project
                                        Name</strong></label>
                                <input type="text" id="project_name" name="project_name"
                                    placeholder="Enter Project Name.." class="form-control">
                                <span class="help-block">Enter a name for this project.</span>
                            </div>
                            <div class="form-group">
                                <label for="project_description" class=" form-control-label"><strong>Project
                                        Description</strong></label>
                                <textarea rows=5 id="project_description" name="project_description"
                                    placeholder="Enter Project Description.." class="form-control"></textarea>
                                <span class="help-block">Describe this project.</span>
                            </div>
                            <div class="form-group">
                                <label for="project_objectives" class=" form-control-label"><strong>Project
                                        Objectives</strong></label>
                                <textarea rows=5 id="project_objectives" name="project_objectives"
                                    placeholder="Enter Project Objectives.." class="form-control"></textarea>
                                <span class="help-block">Enumerate Objectives of this Project</span>
                            </div>
                            </form>
                        </div>
                        <!-- <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </button>
                    </div> -->
                    </div>

                    <!-- Project Resources -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong>Resources</strong>
                                    <div class="small"><i>Add required resources to this Project</i></div>
                                </div>
                                <div class="card-body card-block">
                                    <!-- DATA TABLE -->
                                    <div class="table-data__tool">
                                        <div class="table-data__tool-left"></div>
                                        <div class="table-data__tool-right">
                                            <button id="add_resource"
                                                class="au-btn au-btn-icon au-btn--green au-btn--small">
                                                <i class="zmdi zmdi-plus"></i>Add Resource</button>

                                        </div>
                                    </div>
                                    <div class="table-responsive table-responsive-data2">
                                        <table id="resourcesTable" class="table table-data3">
                                            <thead>
                                                <tr>
                                                    <th>Added by</th>
                                                    <th>Name</th>
                                                    <th>Quantity</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- <tr class="tr-shadow">
                                                    <td>{{ Session::get('FullName') }}</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <input type="text" id="resource_name" name="resource_name"
                                                                placeholder="Resource Name" class="form-control">
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="form-group">
                                                            <input type="text" id="resource_quantity"
                                                                name="resource_quantity" placeholder="Qty"
                                                                class="form-control">
                                                        </div>

                                                    </td>
                                                    <td>
                                                        <div class="table-data-feature">
                                                            <button id="delete" class="item delete"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="Delete">
                                                                <i class="zmdi zmdi-delete"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr> -->
                                                <tr id="no_res_available" class="tr-shadow">
                                                    <td class="denied" colspan=3>No resources available</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- END DATA TABLE -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Project Resources -->
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" name="create_project_submit" id="create_project_submit" class="btn btn-primary">Confirm</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal large -->
<!-- <tr><td>asd</td><td>asd</td><td>asd</td><td>asd</td></tr> -->
<!-- script -->
<script>
    $(document).ready(function () {
        const check_resource_rows = () => {
            var rows = $('#resourcesTable tr').length;
            if (rows <= 2) {
                $('#no_res_available').show();
            } else {
                $('#no_res_available').hide();
            }
        }

        const get_res_json = () => {
                var res = [];
                //$('#tblOne > tbody  > tr').each(function() {...code...});
                $("#resourcesTable > tbody > tr").each(function (index, value) {
                    if (index > 0) {
                        var addedby = $(value).find('td').eq(0).text();
                        var name = $(value).find("#resource_name").val();
                        var quantity = $(value).find("#resource_quantity").val();

                        if ((name.trim() != "") && (quantity.trim() != "")) {
                            res.push({
                                name: name,
                                addedby: addedby,
                                quantity: quantity,
                                cost: 0
                            });

                        } else {
                            toastr.error("Resource Name or Resource Quantity cannot be Empty at Row: " + index, "Index Error");
                        }
                    }
                });

            project_resources = JSON.stringify(res);

            return project_resources;
        }

        $('#add_resource').click(function (e) {
            $('#resourcesTable').append(
                "<tr id=\"res_item\" class=\"tr-shadow\"><td>{{ Session::get('FullName') }}<\/td><td><div class=\"form-group\"><input type=\"text\" id=\"resource_name\" name=\"resource_name\" placeholder=\"Resource Name\" class=\"form-control\">\r\n                                                        <\/div>\r\n                                                    <\/td>\r\n\r\n                                                    <td>\r\n                                                        <div class=\"form-group\">\r\n                                                            <input type=\"text\" id=\"resource_quantity\"\r\n                                                                name=\"resource_quantity\" placeholder=\"Qty\"\r\n                                                                class=\"form-control\">\r\n                                                        <\/div>\r\n\r\n                                                    <\/td>\r\n                                                    <td>\r\n                                                        <div class=\"table-data-feature\">\r\n                                                            <button id=\"delete\" class=\"item delete\"\r\n                                                                data-toggle=\"tooltip\" data-placement=\"top\" title=\"\"\r\n                                                                data-original-title=\"Delete\">\r\n                                                                <i class=\"zmdi zmdi-delete\"><\/i>\r\n                                                            <\/button>\r\n                                                        <\/div>\r\n                                                    <\/td>\r\n                                                <\/tr>"
            );

            check_resource_rows();
        });

        $('#resourcesTable').on('click', "#delete", function () {
            $(this).closest('tr').remove();
            check_resource_rows();
        });

        $('#create_project_submit').click(function (e) {
            project_resources = get_res_json();

            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                url: "{{ url('/oProjects') }}",
                method: 'PUT',
                data: {
                    project_name: $('#project_name').val(),
                    project_description: $('#project_description').val(),
                    project_objectives: $('#project_objectives').val(),
                    project_resources: project_resources
                },
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message, result.title);
                    } else {
                        toastr.error(result.message, result.title);
                    }
                    $('#res_item').remove();
                    $('#create_project').modal('hide');
                    $('.page-container').load('{{ url('/oProjects') }}');
                }
            });
        });
    });
</script>
