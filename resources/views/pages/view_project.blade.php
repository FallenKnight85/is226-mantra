<div class="card">
    <div class="card-header">
        <strong>{{ $ProjectName }}</strong>
        <div class="small"><i>BY: {{ $CompanyName }}</i></div>
    </div>
    <div class="card-body card-block">
        <form id="create_project_form">
            @if(!$edit)
            <div class="form-group">
                <label for="{{ $ProjectID }}_project_name" class=" form-control-label"><strong>Project
                        Name</strong></label>
                <input type="text" id="{{ $ProjectID }}_project_name" name="{{ $ProjectID }}_project_name"
                    placeholder="Enter Project Description.." class="form-control" value="{{ $ProjectName }}"
                    @if(!$edit) readonly @endif>
            </div>
            @else
            <input type="hidden" id="{{ $ProjectID }}_project_name" value="{{ $ProjectName }}">
            @endif
            <div class="form-group">
                <label for="{{ $ProjectID }}_project_description" class=" form-control-label"><strong>Project
                        Description</strong></label>
                <textarea rows=5 id="{{ $ProjectID }}_project_description" name="{{ $ProjectID }}_project_description"
                    placeholder="Enter Project Description.." class="form-control" @if(!$edit) readonly
                    @endif>{{ $ProjectDescription }}</textarea>
            </div>
            <div class="form-group">
                <label for="{{ $ProjectID }}_project_objectives" class=" form-control-label"><strong>Project
                        Objectives</strong></label>
                <textarea rows=5 id="{{ $ProjectID }}_project_objectives" name="{{ $ProjectID }}_project_objectives"
                    placeholder="Enter Project Objectives.." class="form-control" @if(!$edit) readonly
                    @endif>{{ $ProjectObjectives }}</textarea>
            </div>
        </form>
    </div>
</div>

<!-- Project Resources -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong>Resources</strong>
                <div class="small"><i>Add required resources to this Project</i></div>
            </div>
            <div class="card-body card-block">
                <!-- DATA TABLE -->
                @if($edit)
                <div class="table-data__tool">
                    <div class="table-data__tool-left"></div>
                    <div class="table-data__tool-right">
                        <button id="{{ $ProjectID }}_add_resource"
                            class="au-btn au-btn-icon au-btn--green au-btn--small">
                            <i class="zmdi zmdi-plus"></i>Add Resource</button>
                    </div>
                </div>
                @endif
                <div class="table-responsive table-responsive-data2">
                    <table id="{{ $ProjectID }}_resourcesTable" class="table table-data3">
                        <thead>
                            <tr>
                                <th>Added by</th>
                                <th>Name</th>
                                <th>Quantity</th>
                                @if($edit)<th></th>@endif
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="{{ $ProjectID }}_no_res_available" class="tr-shadow">
                                <td class="denied" colspan=3>No resources available</td>
                            </tr>
                            @if (count($resources) > 0)
                            @foreach ($resources as $resource)
                            <tr class="tr-shadow">
                                <!-- extraction int return: {{ extract($resource) }} -->
                                <td>{{ $addedby }}</td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="resource_name" name="resource_name" value="{{ $name }}"
                                            placeholder="Resource Name" class="form-control" @if(!$edit) readonly
                                            @endif>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input type="text" id="resource_quantity" name="resource_quantity"
                                            placeholder="Qty" class="form-control" value="{{ $quantity }}" @if(!$edit)
                                            readonly @endif>
                                    </div>

                                </td>
                                @if($edit)
                                <td>
                                    <div class="table-data-feature">
                                        <button id="delete" class="item delete" data-toggle="tooltip"
                                            data-placement="top" title="" data-original-title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </button>
                                    </div>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- END DATA TABLE -->
            </div>
        </div>
    </div>
</div>
<!-- End Project Resources -->

<script>
    $(document).ready(function () {       
        const check_resource_rows = () => {
            var rows = $('#{{ $ProjectID }}_resourcesTable > tbody > tr').length;
            if (rows <= 2) {
                $('#{{ $ProjectID }}_no_res_available').show();
            } else {
                $('#{{ $ProjectID }}_no_res_available').hide();
            }
        }

        const get_res_json = () => {
                var res = [];
                //$('#tblOne > tbody  > tr').each(function() {...code...});
                $("#{{ $ProjectID }}_resourcesTable > tbody > tr").each(function (index, value) {
                    if (index > 0) {
                        var addedby = $(value).find('td').eq(0).text();
                        var name = $(value).find("#resource_name").val();
                        var quantity = $(value).find("#resource_quantity").val();

                        if ((name.trim() != "") && (quantity.trim() != "")) {
                            res.push({
                                name: name,
                                addedby: addedby,
                                quantity: quantity,
                                cost: 0
                            });

                        } else {
                            toastr.error("Resource Name or Resource Quantity cannot be Empty at Row: " + index, "Index Error");
                        }
                    }
                });

            project_resources = JSON.stringify(res);

            return project_resources;
        }
        @if($edit)
        $('#{{ $ProjectID }}_add_resource').click(function (e) {
            $('#{{ $ProjectID }}_resourcesTable').append(
                "<tr id=\"res_item\" class=\"tr-shadow\"><td>{{ Session::get('FullName') }}<\/td><td><div class=\"form-group\"><input type=\"text\" id=\"resource_name\" name=\"resource_name\" placeholder=\"Resource Name\" class=\"form-control\">\r\n                                                        <\/div>\r\n                                                    <\/td>\r\n\r\n                                                    <td>\r\n                                                        <div class=\"form-group\">\r\n                                                            <input type=\"text\" id=\"resource_quantity\"\r\n                                                                name=\"resource_quantity\" placeholder=\"Qty\"\r\n                                                                class=\"form-control\">\r\n                                                        <\/div>\r\n\r\n                                                    <\/td>\r\n                                                    <td>\r\n                                                        <div class=\"table-data-feature\">\r\n                                                            <button id=\"delete\" class=\"item delete\"\r\n                                                                data-toggle=\"tooltip\" data-placement=\"top\" title=\"\"\r\n                                                                data-original-title=\"Delete\">\r\n                                                                <i class=\"zmdi zmdi-delete\"><\/i>\r\n                                                            <\/button>\r\n                                                        <\/div>\r\n                                                    <\/td>\r\n                                                <\/tr>"
            );

            check_resource_rows();
        });

        $('#{{ $ProjectID }}_resourcesTable').on('click', "#delete", function () {
            $(this).closest('tr').remove();
            check_resource_rows();
        });

        $('#create_view_modal_submit').click(function (e) {
            project_resources = get_res_json();

            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                url: "{{ url('/project') }}\/{{ $ProjectID }}",
                method: 'PUT',
                data: {
                    project_name: $('#{{ $ProjectID }}_project_name').val(),
                    project_description: $('#{{ $ProjectID }}_project_description').val(),
                    project_objectives: $('#{{ $ProjectID }}_project_objectives').val(),
                    project_resources: project_resources
                },
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message, result.title);
                    } else {
                        toastr.error(result.message, result.title);
                    }
                    $('#create_view_modal').modal('hide');
                    $('.page-container').load($('#create_view_modal').find('#create_view_modal_referral').val());
                }
            });
        });

        $('#create_view_modal_submit').show();
        @endif
        check_resource_rows();
    });
</script>
