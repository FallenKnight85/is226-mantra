<div class="card">
    <div class="card-header">
        <strong>Compose Bid</strong>
    </div>
    <div class="card-body card-block">
        <form>
            <div class="form-group">
                <label for="project_id">Project ID</label>
                <input type="text" class="form-control" id="project_id" aria-describedby="project_id_help"
                    value="{{ $project_id }}" readonly>
                <small id="project_id_help" class="form-text text-muted">Share this ID to employees the will be given
                    access to the system.</small>
            </div>
            <div class="form-group">
                <label for="project_name">Project Name</label>
                <input type="text" class="form-control" id="project_name" aria-describedby="project_name_help"
                    value="{{ $project_name }}" readonly>
                <small id="project_name_help" class="form-text text-muted">Share this ID to employees the will be given
                    access to the system.</small>
            </div>
            <div class="form-group">
                <label for="project_by">Posted by</label>
                <input type="text" class="form-control" id="project_by" aria-describedby="project_by_help"
                    value="{{ $project_by }}" readonly>
                <small id="project_by_help" class="form-text text-muted">Share this ID to employees the will be given
                    access to the system.</small>
            </div>
            <div class="form-group">
                <label for="project_description">Project Description</label>
                <textarea rows=5 class="form-control" id="project_description"
                    aria-describedby="project_description_help" readonly>{{ $project_description }}</textarea>
            </div>
            <div class="form-group">
                <label for="project_objectives">Project Objectives</label>
                <textarea rows=5 class="form-control" id="project_objectives" aria-describedby="project_objectives_help"
                    readonly>{{ $project_objectives }}</textarea>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-rouble"></i>
                    </div>
                    <input type="text" class="form-control" id="bid_cost" aria-describedby="bid_cost_help" readonly>
                </div>
            </div>
        </form>
    </div>
    <!-- <div class="card-footer">
        <input name="company_profile_update_link" id="company_profile_update_link" type="hidden"
            value="{{ url('/cProfile') }}">
        <button type="button" name="company_profile_update" id="company_profile_update"
            class="btn btn-primary btn-sm">Create New Task</button>
        <button type="button" name="company_profile_update" id="company_profile_update"
            class="btn btn-primary btn-sm">Submit Bid</button>
        <div class="small"><i>Note: Submitted Bid can still be edited if not yet approved.</i></div>
    </div> -->
</div>

<!-- Project Resources -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong>Resources</strong>
                <div class="small"><i>Supply Cost the following resources specified by the Client</i></div>
            </div>
            <div class="card-body card-block">
                <!-- DATA TABLE -->
                <div class="table-responsive table-responsive-data2">
                    <table id="{{ $ProjectID }}_resourcesTable" class="table table-data3">
                        <thead>
                            <tr>
                                <th>Resource</th>
                                <th>Quantity</th>
                                <th>Unit Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="{{ $ProjectID }}_no_res_available" class="tr-shadow">
                                <td class="denied" colspan=3>No resources available</td>
                            </tr>
                            @if (count($resources) > 0)
                                @foreach ($resources as $resource)
                                <tr class="tr-shadow">
                                    <!-- extraction int return: {{ extract($resource) }} -->
                                    <input type="hidden" id="resource_addedby" value="{{ $addedby }}">
                                    <td>
                                        <div class="form-group">
                                            <input type="text" id="resource_name" name="resource_name" value="{{ $name }}"
                                                placeholder="Resource Name" class="form-control" readonly>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="form-group">
                                            <input type="text" id="resource_quantity" name="resource_quantity"
                                                placeholder="Qty" class="form-control" value="{{ $quantity }}" readonly>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="form-group">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-rouble"></i>
                                                    </div>

                                                    <input type="text" id="resource_cost" name="resource_cost"
                                                        placeholder="Unit Cost" class="form-control" value="{{ $cost }}"
                                                        @if(!$edit) readonly @endif>
                                                </div>
                                            </div>
                                    </td>
                                </tr>
                                @endforeach

                                <tr class="tr-shadow">
                                    <td colspan=2>
                                        <div class="text-right">
                                            <strong>Total Cost :</strong>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-rouble"></i>
                                                </div>
                                                <input type="text" id="strong-total-cost" name="strong-total-cost"
                                                    class="form-control" readonly>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- END DATA TABLE -->
            </div>
            @if($bid_status == 'MOpen')
                @if(Session::get('isManager') == 'true')
                <div class="card-footer">
                    <button class="btn btn-primary" onclick="change_bid_status('{{ $bid_id }}', '', 'Open');">Manager Approval</button>
                </div>
                @else
                    Waiting for Manager Approval for this bid...
                @endif
            @endif
        </div>
    </div>
</div>
<!-- End Project Resources -->
<script>
    $(document).ready(function () {
        const check_resource_rows = () => {
            var rows = $('#{{ $ProjectID }}_resourcesTable > tbody > tr').length;
            if (rows <= 2) {
                $('#{{ $ProjectID }}_no_res_available').show();
            } else {
                $('#{{ $ProjectID }}_no_res_available').hide();
            }
        }

        const get_res_json = () => {
            var res = [];
            var total_cost = 0;
            var rows = $('#{{ $ProjectID }}_resourcesTable > tbody > tr').length;
            $("#{{ $ProjectID }}_resourcesTable > tbody > tr").each(function (index, value) {
                if (index > 0 && index < rows - 1) {
                    var addedby = $(value).find("#resource_addedby").val();
                    var name = $(value).find("#resource_name").val();
                    var quantity = $(value).find("#resource_quantity").val();
                    var cost = $(value).find("#resource_cost").val();

                    if (cost.trim() != '') {
                        if (($.isNumeric(cost)) && (cost >= 0)) {
                            total_cost += parseInt(cost) * parseInt(quantity);
                            res.push({
                                name: name,
                                addedby: addedby,
                                quantity: quantity,
                                cost: cost
                            });
                        } else {
                            toastr.error("Resource must be non-negative floating point at Row: " + index, "Index Error");
                        }

                    } else {
                        toastr.error("Resource cost cannot be Empty at Row: " + index, "Index Error");
                    }
                }
            });

            project_resources = JSON.stringify(res);
            return {
                json: project_resources,
                cost: total_cost
            };
        }

        const show_total_cost = () => {
            var bid_res = get_res_json();
            $("#{{ $ProjectID }}_resourcesTable > tbody").find("#strong-total-cost").val(bid_res.cost);
            $("#bid_cost").val(bid_res.cost);
        }

        // Include this part if editable
        @if($edit)        
        $('#{{ $ProjectID }}_resourcesTable').on('change', ".form-control", function () {
            show_total_cost();
        });

        $('#create_view_modal_submit').click(function (e) {
            var bid_res = get_res_json();

            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
           
            $.ajax({
                url: "{{ url('/bid') }}",
                method: "{{ $method }}",
                data: {
                    for_company_id: "{{ $company_id }}",
                    for_project_id: "{{ $project_id }}",
                    project_name: "{{ $project_name }}",
                    company_name: "{{ $project_by }}",
                    resource: bid_res.json,
                    cost: bid_res.cost
                },
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message, result.title);
                    } else {
                        toastr.error(result.message, result.title);
                    }
                    $('#create_view_modal').modal('hide');
                    $('.page-container').load($('#create_view_modal').find('#create_view_modal_referral').val());
                }
            });
        });
        $('#create_view_modal_submit').show();
        @endif
        //

        check_resource_rows();
        show_total_cost();
    });
</script>

