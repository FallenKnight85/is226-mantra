<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <strong>User</strong> Profile
                </div>
                <div class="card-body card-block">
                    <form>
                        <div class="row">
                            <div class="text-center col-md-12">
                                <a href="#">
                                    <img class="rounded-circle mx-auto d-block" src="/avatars/{{ $avatar }}"
                                        alt="Card image cap">
                                    <!-- <img src="/avatars/{{ $avatar }}" alt="{{ $nickname }}" /> -->
                                </a>
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="nickname">Nick Name</label>
                                <input type="text" class="form-control" id="nickname" aria-describedby="nickname_help"
                                    value="{{ $NickName }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="last_name">Lastname</label>
                                <input type="text" class="form-control" id="last_name" aria-describedby="last_name_help"
                                    value="{{ $LastName }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="given_name">Given Name</label>
                                <input type="text" class="form-control" id="given_name"
                                    aria-describedby="given_name_help" value="{{ $GivenName }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="suffix">Suffix</label>
                                <input type="text" class="form-control" id="suffix" aria-describedby="suffix_help"
                                    value="{{ $Suffix }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="gender">Gender</label>
                                <select name="gender" id="gender" class="form-control" aria-describedby="gender_help">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="birth_date" class="control-label">Birth Date</label>
                                <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd "
                                    data-link-field="birth_date" data-link-format="yyyy-mm-dd">
                                    <input class="form-control" size="16" type="text" value="{{ $BirthDate }}" readonly>
                                    <span class="input-group-addon"><span
                                            class="fas fa-times glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span
                                            class="fas fa-calendar-alt glyphicon-calendar"></span></span>
                                </div>
                                <input type="hidden" id="birth_date" name="birth_date" value="{{ $BirthDate }}" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="mailing_address">Mailing Address</label>
                                <textarea class="form-control" id="mailing_address"
                                    aria-describedby="mailing_address_help">{{ $MailingAddress }}</textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="telephone_number">Telephone Number</label>
                                <input type="text" class="form-control" id="telephone_number"
                                    aria-describedby="telephone_number_help" value="{{ $TelephoneNumber }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="mobile_number">Mobile Number</label>
                                <input type="text" class="form-control" id="mobile_number"
                                    aria-describedby="mobile_number_help" value="{{ $MobileNumber }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="nationality">Nationality</label>
                                <input type="text" class="form-control" id="nationality"
                                    aria-describedby="nationality_help" value="{{ $Nationality }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="country">Country</label>
                                <input type="text" class="form-control" id="country" aria-describedby="country_help"
                                    value="{{ $Country }}">
                            </div>

                        </div>
                    </form>
                </div>


                <div class="card-footer">
                    <input name="user_profile_update_link" id="user_profile_update_link" type="hidden"
                        value="{{ url('/cProfile') }}">
                    <button type="button" name="user_profile_update" id="user_profile_update"
                        class="btn btn-primary">Update User Profile</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- script -->
<script>
    $('.form_date').datetimepicker({
        fontAwesome: 1,
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $(document).ready(function(){
        $('#user_profile_update').click(function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                url: '{{ url('/profile') }}\/{{ $ProfileID }}',
                method: 'PUT',
                data: {
                    last_name: $('#last_name').val(),
                    given_name: $('#given_name').val(),
                    suffix: $('#suffix').val(),
                    nickname: $('#nickname').val(),
                    mailing_address: $('#mailing_address').val(),
                    gender: $('#gender').val(),
                    telephone_number: $('#telephone_number').val(),
                    mobile_number: $('#mobile_number').val(),
                    nationality: $('#nationality').val(),
                    country: $('#country').val(),
                    birth_date: $('#birth_date').val(),
                },
                success: function(result){
                    if(result.status == 'success') {
                        toastr.success(result.message, result.title);
                    } else {
                        toastr.error(result.message, result.title);
                    }
                    navigate('{{ url('/profile') }}\/{{ $ProfileID }}');
                }
            });
        });
    });
</script>
