<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Manage Tasks for {{ $project_name }}</strong>
                            <input type="hidden" value="{{ $project_id }}">
                            <div class="small"><i>{{ $project_by }}</i></div>
                        </div>
                        <div class="card-body card-block">
                            <form>
                                <div class="form-group">
                                    <label for="project_description">Project Description</label>
                                    <textarea rows=5 class="form-control" id="project_description" aria-describedby="project_description_help" readonly>{{ $project_description }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="project_objectives">Project Objectives</label>
                                    <textarea rows=5 class="form-control" id="project_objectives" aria-describedby="project_objectives_help" readonly>{{ $project_objectives }}</textarea>
                                </div>                        
                            </form>
                        </div>
                        <div class="card-footer">
                            @if($owned == 'false')
                            <button type="button" class="btn btn-primary btn-sm" onclick="create_view_modal('{{ $task_create_uri }}', '{{ Request::url() }}');">Create New Task</button>
                            @endif
                            <div class="small"><i>Note: if Subtask is approved, edits will not be allowed.</i></div>
                        </div>
                    </div>    
                </div>
            </div>
            

            <!-- Bid Tasks -->
            @if(count($tasks) > 0)
            @foreach($tasks as $task)
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <!-- {{ extract($task) }} -->
                            <strong>{{ $TaskName }}</strong>
                            @if($owned == 'false')
                            <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" onclick="create_view_modal('{{ $task_edit_uri }}', '{{ Request::url() }}');">
                                <i class="zmdi zmdi-edit"></i>
                            </button>
                            <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="delete_task('{{ $task_delete_uri }}', '{{ Request::url() }}');">
                                <i class="zmdi zmdi-delete"></i>
                            </button>
                            @endif
                            <div class="small"><i>{{ $TaskDescription }}</i></div>
                        </div>
                        <div class="card-body card-block">
                            <!-- DATA TABLE -->
                            @if($owned == 'false')
                            <div class="table-data__tool">
                                    <div class="table-data__tool-left"></div>
                                    <div class="table-data__tool-right">
                                        @if($TaskStatus == 'MOpen')
                                            @if(Session::get('isManager') == 'true')
                                                <button class="btn btn-primary" onclick="change_task_status('{{ $TaskID }}','{{ Request::url() }}', 'Open');">Manager Approval</button>
                                            @else
                                                Waiting for Manager Approval to add Milestones...
                                            @endif
                                        @elseif($TaskStatus == 'Open')
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small" onclick="create_view_modal('{{ $subtask_create_uri }}', '{{ Request::url() }}');">
                                            <i class="zmdi zmdi-plus"></i>create sub task</button>
                                        @endif
                                    </div>
                            </div>
                            @endif
                            <div class="table-responsive table-responsive-data3">
                                <table class="table table-data3">
                                    <thead>
                                        <tr>
                                            <th>Added by</th>
                                            <th>Subtask Details</th>
                                            <th>Target Date</th>
                                            <th>Date Completed</th>
                                            <th>
                                                
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($subtasks) > 0)
                                        @foreach($subtasks as $subtask)
                                        <tr class="tr-shadow">
                                            <td>Test USER</td>
                                            <td>{{ $subtask->SubtaskDetails }}</td>
                                            <td>{{ $subtask->TargetDate }}</td>
                                            <td>{{ $subtask->DateCompleted }}</td>
                                            <td>
                                                @if($owned == 'true')
                                                    @if($subtask->SubTaskStatus == 'MOpen')
                                                    Waiting Submit
                                                    @elseif($subtask->SubTaskStatus == 'Open')
                                                    Waiting Submit
                                                    @elseif($subtask->SubTaskStatus == 'MSubmit')
                                                    Waiting Submit
                                                    @elseif($subtask->SubTaskStatus == 'Submit')
                                                    <button class="btn btn-primary" onclick="change_subtask_status('{{ $subtask->SubTaskID }}','{{ Request::url() }}', 'MApproved');">Approve</button>
                                                    @elseif($subtask->SubTaskStatus == 'MApproved')
                                                        @if(Session::get('isManager') == 'true')
                                                            <button class="btn btn-primary" onclick="change_subtask_status('{{ $subtask->SubTaskID }}','{{ Request::url() }}', 'Approved');">Manager Approval</button>
                                                        @else
                                                            <div class="text-center text-primary">Waiting for Manager Approval</div>
                                                        @endif
                                                    @elseif($subtask->SubTaskStatus == 'Approved')
                                                    Waiting Done
                                                    @elseif($subtask->SubTaskStatus == 'MDone')
                                                    Waiting Done
                                                    @elseif($subtask->SubTaskStatus == 'Done')
                                                    <button class="btn btn-primary" onclick="change_subtask_status('{{ $subtask->SubTaskID }}','{{ Request::url() }}', 'MClosed');">Close</button>
                                                    @elseif($subtask->SubTaskStatus == 'MClosed')
                                                        @if(Session::get('isManager') == 'true')
                                                            <button class="btn btn-primary" onclick="change_subtask_status('{{ $subtask->SubTaskID }}','{{ Request::url() }}', 'Closed');">Manager Approval</button>
                                                        @else
                                                            <div class="text-center text-primary">Waiting for Manager Approval</div>
                                                        @endif
                                                    @elseif($subtask->SubTaskStatus == 'Closed')
                                                    <div class="process">Closed</div>
                                                    @endif
                                                @else
                                                    @if($subtask->SubTaskStatus == 'MOpen')
                                                        @if(Session::get('isManager') == 'true')
                                                            <button class="btn btn-primary" onclick="change_subtask_status('{{ $subtask->SubTaskID }}','{{ Request::url() }}', 'Open');">Manager Approval</button>
                                                        @else
                                                            <div class="text-center text-primary">Waiting for Manager Approval</div>
                                                        @endif
                                                    @elseif($subtask->SubTaskStatus == 'Open')
                                                    <div class="table-data-feature">
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" onclick="create_view_modal('{{ $subtask->subtask_edit_uri }}', '{{ Request::url() }}');">
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="delete_subtask('{{ $subtask->subtask_delete_uri }}', '{{ Request::url() }}');">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Submit" onclick="change_subtask_status('{{ $subtask->SubTaskID }}','{{ Request::url() }}', 'MSubmit');">
                                                            <i class="zmdi zmdi-mail-send"></i>
                                                        </button>
                                                    </div>
                                                    @elseif($subtask->SubTaskStatus == 'MSubmit')
                                                        @if(Session::get('isManager') == 'true')
                                                            <button class="btn btn-primary" onclick="change_subtask_status('{{ $subtask->SubTaskID }}','{{ Request::url() }}', 'Submit');">Manager Approval</button>
                                                        @else
                                                            <div class="text-center text-primary">Waiting for Manager Approval</div>
                                                        @endif
                                                    @elseif($subtask->SubTaskStatus == 'Submit')
                                                    Waiting Approval
                                                    @elseif($subtask->SubTaskStatus == 'MApproved')
                                                    Waiting Approval
                                                    @elseif($subtask->SubTaskStatus == 'Approved')
                                                    <button class="btn btn-primary" onclick="change_subtask_status('{{ $subtask->SubTaskID }}','{{ Request::url() }}', 'MDone');">Done</button>
                                                    @elseif($subtask->SubTaskStatus == 'MDone')
                                                        @if(Session::get('isManager') == 'true')
                                                            <button class="btn btn-primary" onclick="change_subtask_status('{{ $subtask->SubTaskID }}','{{ Request::url() }}', 'Done');">Manager Approval</button>
                                                        @else
                                                            <div class="text-center text-primary">Waiting for Manager Approval</div>
                                                        @endif
                                                    @elseif($subtask->SubTaskStatus == 'Done')
                                                    Waiting Verify
                                                    @elseif($subtask->SubTaskStatus == 'MClosed')
                                                    Waiting Verify
                                                    @elseif($subtask->SubTaskStatus == 'Closed')
                                                    <div class="text-center text-success">Closed</div>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr class="tr-shadow">
                                            <td class="denied" colspan=3>No subtasks available</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
            <!-- End Bid Tasks -->
        </div>
    </div>
</div>

<!-- script -->
<script>
    // $('#company_profile_update').click(function (e) {
    //     e.preventDefault();
    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    //         }
    //     });
        
    //     $.ajax({
    //         url: $('#company_profile_update_link').val(),
    //         method: 'post',
    //         data: {
    //             business_name: $('#business_name').val(),
    //             business_address: $('#business_address').val(),
    //             business_telephone: $('#business_telephone').val(),
    //             business_tags: $('#business_tags').val()
    //         },
    //         success: function(result){
    //             if(result.status == 'success') {
    //                 toastr.success(result.message, result.title);
    //             } else {
    //                 toastr.error(result.message, result.title);
    //             }
    //         }
    //     });
    // });
</script>