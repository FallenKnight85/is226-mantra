<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-1">List of Owned Projects</h2>
                        <button class="au-btn au-btn-icon au-btn--blue" name="post_new_project" data-toggle="modal" data-target="#create_project">
                            <i class="zmdi zmdi-plus"></i>Post New Project</button>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                @if (count($projects))
                    @foreach ($projects as $project)
                        <div class="col-md-4" name="{{ $project->ProjectID }}_card" id="{{ $project->ProjectID }}_card">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-user"></i>
                                    <strong class="card-title pl-2">{{ $project->ProjectName }}</strong><span class="badge badge-primary">Open</span>
                                    @if($project->bid_count > 0)
                                    <span class="badge badge-danger">{{ $project->bid_count }} Bids Avail</span>
                                    @endif
                                </div>
                                <div class="card-body">
                                    <div class="mx-auto d-block">
                                        {{ $project->ProjectDescription }}
                                    </div>
                                    <hr>
                                    <div class="card-text text-sm-right">
                                        @if($project->bid_count > 0)
                                        <button type="button" class="btn btn-primary btn-sm" onclick="create_view_modal('{{ url('/project') }}/?{{ $project->query }}', '{{ Request::url() }}');">View Bids</button>
                                        @endif
                                        <button type="button" class="btn btn-danger btn-sm" onclick="delete_project('{{ $project->ProjectID }}', '{{ Request::url() }}');">Cancel Project</button>
                                        <button type="button" class="btn btn-success btn-sm" onclick="create_view_modal('{{ url('/project').'/'.$project->ProjectID }}/edit', '{{ Request::url() }}');">Edit Project</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT-->

@include('modals.create_project')