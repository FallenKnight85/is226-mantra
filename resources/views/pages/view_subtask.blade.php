<div class="card">
    <div class="card-header">
        Create Subtask for <strong>{{ $TaskName }}</strong> of <strong>{{ $ProjectName }}</strong>
        <div class="small"><i>BY: {{ $CompanyName }}</i></div>
    </div>
    <div class="card-body card-block">
        <form>
            <input type="hidden" id="{{ $TaskID }}">
            <div class="form-group">
                <label for="subtask_details" class=" form-control-label"><strong>Subtask
                        Details</strong></label>
                <textarea rows=5 id="subtask_details" name="subtask_details" placeholder="Enter Task Details.."
                    class="form-control" @if(!$edit) readonly @endif>{{ $SubtaskDetails }}</textarea>
            </div>
            <div class="form-group">
                <label for="target_date" class="control-label">Target Date</label>
                <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd "
                    data-link-field="target_date" data-link-format="yyyy-mm-dd">
                    <input class="form-control" size="16" type="text" value="{{ $TargetDate }}" readonly>
                    <span class="input-group-addon"><span class="fas fa-times glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="fas fa-calendar-alt glyphicon-calendar"></span></span>
                </div>
                <input type="hidden" id="target_date" name="target_date" value="{{ $TargetDate }}" />
            </div>
        </form>
    </div>
</div>

<script>
$('.form_date').datetimepicker({
    fontAwesome: 1,
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
});

@if($edit)
$('#create_view_modal_submit').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $.ajax({
        url: '{{ $uri }}',
        method: '{{ $method }}',
        data: {
            task_id: '{{ $TaskID }}',
            subtask_details: $('#subtask_details').val(),
            target_date: $('#target_date').val()
        },
        success: function (result) {
            if (result.status == 'success') {
                toastr.success(result.message, result.title);
            } else {
                toastr.error(result.message, result.title);
            }
            $('#create_view_modal').modal('hide');
            $('.page-container').load($('#create_view_modal').find('#create_view_modal_referral').val());
        }
    });
});

$('#create_view_modal_submit').show();
@endif
</script>
