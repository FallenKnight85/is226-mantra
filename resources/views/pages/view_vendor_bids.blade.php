<!-- Project Resources -->
@foreach($bids as $bid)
<!-- extraction int return: {{ extract($bid) }} -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong>Bid No. {{ $index }}</strong>
                <div class="small"><i>BY: {{ $company_name }}</i></div>
            </div>
            <div class="card-body card-block">
                <!-- DATA TABLE -->
                <div class="table-responsive table-responsive-data2">
                    <table id="{{ $BidID }}_resourcesTable" class="table table-data3">
                        <thead>
                            <tr>
                                <th>Added By</th>
                                <th>Resource</th>
                                <th>Quantity</th>
                                <th>Unit Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($resources) > 0)
                                @foreach ($resources as $resource)
                                <tr class="tr-shadow">
                                    <!-- extraction int return: {{ extract($resource) }} -->
                                    <td>
                                        {{ $addedby }}
                                    </td>
                                    <td>
                                        {{ $name }}
                                    </td>
                                    <td>
                                        {{ $quantity }}
                                    </td>

                                    <td>
                                        <div class="form-group">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-rouble"></i>
                                                    </div>

                                                    <input type="text" id="resource_cost" name="resource_cost"
                                                        placeholder="Unit Cost" class="form-control" value="{{ $cost }}" readonly>
                                                </div>
                                            </div>
                                    </td>
                                </tr>
                                @endforeach

                                <tr class="tr-shadow">
                                    <td colspan=3>
                                        <div class="text-right">
                                            <strong>Total Cost :</strong>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-rouble"></i>
                                                </div>
                                                <input type="text" id="strong-total-cost" name="strong-total-cost"
                                                    class="form-control" value="{{ $total_cost }}" readonly>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @else
                                <tr id="{{ $BidID }}_no_res_available" class="tr-shadow">
                                    <td class="denied" colspan=3>No resources available</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- END DATA TABLE -->
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <button type="button" class="btn btn-primary" onclick="approve_bid('{{ $BidID }}', '{{ url('/xProjects') }}');">Approve Bid</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- End Project Resources -->