<div class="card">
    <div class="card-header">
        <strong>Create Task For: {{ $ProjectName }}</strong>
        <div class="small"><i>BY: {{ $CompanyName }}</i></div>
    </div>
    <div class="card-body card-block">
        <form>
            <input type="hidden" id="{{ $TaskID }}">
            <div class="form-group">
                <label for="task_name" class=" form-control-label"><strong>Task Name</strong></label>
                <input type="text" id="task_name" name="task_name"
                    placeholder="Enter Task Name.." class="form-control" value="{{ $TaskName }}"
                    @if(!$edit) readonly @endif>
            </div>
            <div class="form-group">
                <label for="task_description" class=" form-control-label"><strong>Task
                        Description</strong></label>
                <textarea rows=5 id="task_description" name="task_description"
                    placeholder="Enter Task Description.." class="form-control" @if(!$edit) readonly
                    @endif>{{ $TaskDescription }}</textarea>
            </div>
        </form>
    </div>
</div>

@if($edit)
<script>
    $(document).ready(function () {       
        $('#create_view_modal_submit').click(function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                url: '{{ $uri }}',
                method: '{{ $method }}',
                data: {
                    project_id: '{{ $ProjectID }}',
                    task_name: $('#task_name').val(),
                    task_description: $('#task_description').val()
                },
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message, result.title);
                    } else {
                        toastr.error(result.message, result.title);
                    }
                    $('#create_view_modal').modal('hide');
                    $('.page-container').load($('#create_view_modal').find('#create_view_modal_referral').val());
                }
            });
        });

        $('#create_view_modal_submit').show();
    });
</script>
@endif