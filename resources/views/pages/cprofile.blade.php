<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <strong>Company</strong> Profile
                </div>
                <div class="card-body card-block">
                    <form>
                        <div class="form-group">
                            <label for="business_id">Company ID</label>
                            <input type="text" class="form-control" id="business_id" aria-describedby="business_id_help" value="{{ $business_id }}" readonly>
                            <small id="business_id_help" class="form-text text-muted">Share this ID to employees the will be given access to the system.</small>
                        </div>
                        <div class="form-group">
                            <label for="business_name">Business Name</label>
                            <input type="text" class="form-control" id="business_name" aria-describedby="business_name_help" value="{{ $business_name }}" @if($owner == 'false')readonly @endif>
                        </div>
                        <div class="form-group">
                            <label for="business_address">Business Address</label>
                            <input type="text" class="form-control" id="business_address" aria-describedby="business_address_help" value="{{ $business_address }}" @if($owner == 'false')readonly @endif>
                        </div>
                        <div class="form-group">
                            <label for="business_telephone">Business Telephone</label>
                            <input type="text" class="form-control" id="business_telephone" aria-describedby="business_telephone_help" value="{{ $business_telephone }}" @if($owner == 'false')readonly @endif>
                        </div>
                        <div class="form-group">
                            <label for="business_tags">Business Tags</label>
                            <input type="text" class="form-control" id="business_tags" aria-describedby="business_tags_help" value="{{ $business_tags }}" @if($owner == 'false')readonly @endif>
                            <small id="business_tags_help" class="form-text text-muted">Comma-separated key words.</small>
                        </div>
                        <!-- <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div> -->
                        
                    </form>
                </div>
                @if($owner == 'true')
                <div class="card-footer">
                    <input name="company_profile_update_link" id="company_profile_update_link" type="hidden" value="{{ url('/cProfile') }}">
                    <button type="button" name="company_profile_update" id="company_profile_update" class="btn btn-primary">Update Company Profile</button>
                </div>
                @endif
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h2 class="title-1 m-b-25">Users</h2>
                    <div class="table-responsive table--no-card m-b-40">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email Name</th>
                                    <th>Role</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($profiles) > 0)
                                @foreach($profiles as $profile)
                                <tr>
                                    <td>{{ $profile->GivenName }} {{ $profile->LastName }}</td>
                                    <td>{{ $profile->EmailAddress }}</td>
                                    <td>
                                        @if($profile->o)
                                            Manager
                                        @else
                                            Normal
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr class="tr-shadow">
                                    <td class="denied" colspan=3>No users available</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if($owner == 'true')
<!-- script -->
<script>
    $('#company_profile_update').click(function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        
        $.ajax({
            url: $('#company_profile_update_link').val(),
            method: 'post',
            data: {
                business_name: $('#business_name').val(),
                business_address: $('#business_address').val(),
                business_telephone: $('#business_telephone').val(),
                business_tags: $('#business_tags').val()
            },
            success: function(result){
                if(result.status == 'success') {
                    toastr.success(result.message, result.title);
                } else {
                    toastr.error(result.message, result.title);
                }
            }
        });
    });
</script>
@endif