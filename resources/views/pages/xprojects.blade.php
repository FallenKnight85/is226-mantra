<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-1">List of Current Projects</h2>
                        <button class="au-btn au-btn-icon au-btn--blue" name="post_new_project" data-toggle="modal" data-target="#create_project">
                            <i class="zmdi zmdi-plus"></i>Post New Project</button>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Company Owned Approved Projects
                        </div>
                        <div class="card-body card-block">

                            @if (count($approvedOwnedProjects))
                            @foreach ($approvedOwnedProjects as $project)
                            <div class="row">
                                <div class="col-md-12" name="{{ $project->ProjectID }}_card" id="{{ $project->ProjectID }}_card">
                                    <div class="card">
                                        <div class="card-header">
                                            <i class="fa fa-user"></i>
                                            <strong class="card-title pl-2">{{ $project->ProjectName }}</strong>
                                            @if($project->bid_status=='Approved')
                                                <span class="badge badge-primary">
                                            @else
                                                <span class="badge badge-danger">
                                            @endif
                                                {{ $project->bid_status }}
                                                </span>
                                                <span class="badge badge-danger">Delayed</span>
                                            <div class="small"><i>Project being done by: {{ $project->company_name }}</i></div>
                                        </div>
                                        <div class="card-body">
                                            <div class="mx-auto d-block">
                                                <div class="text-left">
                                                    Current Project Progress:
                                                </div>
                                                <div class="progress mb-2">
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ $project->project_progress->project_progress }}%" aria-valuenow="{{ $project->project_progress->project_progress }}" aria-valuemin="0" aria-valuemax="100">{{ $project->project_progress->project_progress }}%</div>
                                                </div>
                                                <div class="text-right small">
                                                    <i>{{ $project->project_progress->count_tasks_completed }} / {{ $project->project_progress->count_tasks }} Tasks and {{ $project->project_progress->count_subtasks_total_closed }} / {{ $project->project_progress->count_subtasks_total }} Subtasks Completed</i>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="card-text text-sm-right">
                                                <button type="button" class="btn btn-primary btn-sm" onclick="create_view_modal('{{ $project->gantt_uri }}','');">View Gantt</button>
                                                <button type="button" class="btn btn-primary btn-sm item-nav" c="{{ url('/task') }}/{{ $project->ProjectID }}">Manage Tasks</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-center text-danger">
                                        No data available...
                                    </div>
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Client Owned Approved Projects
                        </div>
                        <div class="card-body card-block">

                            @if (count($approvedProjects))
                            @foreach ($approvedProjects as $project)
                            <div class="row">
                                <div class="col-md-12" name="{{ $project->ProjectID }}_card" id="{{ $project->ProjectID }}_card">
                                    <div class="card">
                                        <div class="card-header">
                                            <i class="fa fa-user"></i>
                                            <strong class="card-title pl-2">{{ $project->ProjectName }} </strong>
                                            @if($project->bid_status=='Approved')
                                                <span class="badge badge-primary">
                                            @else
                                                <span class="badge badge-danger">
                                            @endif
                                                {{ $project->bid_status }}
                                                </span>
                                                <span class="badge badge-danger">Delayed</span>
                                            <div class="small"><i>Project being done for: {{ $project->company_name }}</i></div>
                                        </div>
                                        <div class="card-body">
                                            <div class="mx-auto d-block">
                                                <div class="text-left">
                                                    Current Project Progress:
                                                </div>
                                                <div class="progress mb-2">
                                                <div class="progress-bar bg-success" role="progressbar" style="width: {{ $project->project_progress->project_progress }}%" aria-valuenow="{{ $project->project_progress->project_progress }}" aria-valuemin="0" aria-valuemax="100">{{ $project->project_progress->project_progress }}%</div>
                                                </div>
                                                <div class="small text-right">
                                                    <i>{{ $project->project_progress->count_tasks_completed }} / {{ $project->project_progress->count_tasks }} Tasks and {{ $project->project_progress->count_subtasks_total_closed }} / {{ $project->project_progress->count_subtasks_total }} Subtasks Completed</i>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="card-text text-sm-right">
                                                <button type="button" class="btn btn-primary btn-sm" onclick="create_view_modal('{{ $project->gantt_uri }}','');">View Gantt</button>
                                                <button type="button" class="btn btn-primary btn-sm item-nav" c="{{ url('/task') }}/{{ $project->ProjectID }}">Manage Tasks</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-center text-danger">
                                        No data available...
                                    </div>
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>
            
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT-->

@include('modals.create_project')