<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-1">List of Available Projects</h2>
                        <button class="au-btn au-btn-icon au-btn--blue" name="post_new_project" data-toggle="modal" data-target="#create_project">
                            <i class="zmdi zmdi-plus"></i>Post New Project</button>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                @if (count($projects))
                    @foreach ($projects as $project)
                        <div class="col-md-4" name="{{ $project->ProjectID }}_card" id="{{ $project->ProjectID }}_card">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-user"></i>
                                    <strong class="card-title pl-2"> {{ $project->ProjectName }}</strong><span class="badge badge-primary">Open</span>
                                    <h6><i>BY: {{ $project->CompanyName }}</i></h6>
                                </div>
                                <div class="card-body">
                                    <div class="mx-auto d-block">
                                        {{ $project->ProjectDescription }}
                                    </div>
                                    <hr>
                                    <div class="card-text text-sm-right">
                                        @if ($project->me_hasbid != 'true')
                                        <button type="button" class="btn btn-primary btn-sm" onclick="create_view_modal('{{ url('/bid/create') }}/?{{ $project->query }}', '{{ Request::url() }}');">Compose Bid</button>
                                        @else
                                        <button type="button" class="btn btn-primary btn-sm" onclick="create_view_modal('{{ url('/bid').'/'.$project->me_BidID }}/edit');">View Bid</button>
                                        <button type="button" class="btn btn-danger btn-sm" onclick="delete_bid('{{ $project->me_BidID }}', '{{ Request::url() }}');">Delete Bid</button>
                                        @endif
                                        <button type="button" class="btn btn-success btn-sm" onclick="create_view_modal('{{ url('/project').'/'.$project->ProjectID }}', '{{ Request::url() }}');">View Project</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT-->

@include('modals.create_project')
@include('modals.create_bid')