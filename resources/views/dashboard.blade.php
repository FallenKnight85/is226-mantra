@extends('theme.default')
@section('content')
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">overview ({{ $business_name }})</h2>
                                        <button class="au-btn au-btn-icon au-btn--blue" name="post_new_project" data-toggle="modal" data-target="#create_project">
                                            <i class="zmdi zmdi-plus"></i>Post New Project</button>    
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-25">
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c1">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-account-o"></i>
                                            </div>
                                            <div class="text">
                                                <h2>{{ $countApproved }}</h2>
                                                <span>Projects On-going</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c2">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-shopping-cart"></i>
                                            </div>
                                            <div class="text">
                                                <h2>{{ $countCompleted }}</h2>
                                                <span>Projects Completed</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c3">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="text">
                                                <h2>5 stars</h2>
                                                <span>Customer Satisfaction</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c4">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="fa fa-rub"></i>
                                            </div>
                                            <div class="text">
                                                <h2>1,060,386</h2>
                                                <span>total earnings</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>


                        <div class="row">
                            <div class="col-lg-12">
                                <h2 class="title-1 m-b-25">Top 10 Projects Status</h2>
                                <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                <th>Project Name</th>
                                                <th>Company Name</th>
                                                <th>Progress</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($projects) > 0)
                                            @foreach($projects as $project)
                                            <tr>
                                                <td>{{ $project['ProjectName'] }}</td>
                                                <td>{{ $project['company_name'] }}</td>
                                                <td>
                                                    <div class="progress mb-2">
                                                        <!-- {{ $prog = $project['project_progress']->project_progress }}   -->
                                                        <div class="progress-bar bg-success" role="progressbar" style="width: {{ $prog }}%" aria-valuenow="{{ $prog }}" aria-valuemin="0" aria-valuemax="100">{{ $prog }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr class="tr-shadow">
                                                <td class="denied" colspan=3>No Projects to show</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2019 Capispisan; Libiano. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->

            @include('modals.create_project')
@endsection