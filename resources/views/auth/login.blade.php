@extends('theme.defaultlogin')
@section('content')
<div class="page-content--bge5">
    <div class="container">
        <div class="login-wrap">
            <div class="login-content">
                <div class="login-logo">
                    <a href="#">
                        <img src="images/mantra-logo.png" alt="Mantra">
                    </a>
                </div>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <div class="login-form">
                    <form role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Email Address</label>
                            <input id="email" type="email" class="au-input au-input--full" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input id="password" type="password" class="au-input au-input--full" name="password" placeholder="Password" required>
                        </div>
                        <div class="login-checkbox">
                            <label>
                                <input type="checkbox" name="remember">Remember Me
                            </label>
                            <label>
                                <a href="{{ route('password.request') }}">Forgotten Password?</a>
                            </label>
                        </div>
                        <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
                    </form>
                    <div class="register-link">
                        <p>
                            Don't you have account?
                            <a href="{{ url('/register') }}">Sign Up Here</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection