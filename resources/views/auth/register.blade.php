<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="_token" content="{{ csrf_token() }}" />
	<meta name="_s1_validator" content="{{ url('/register/step1') }}" />
	<meta name="_s2_validator" content="{{ url('/register/step2') }}" />
	<meta name="_s3_validator" content="{{ url('/register/step3') }}" />
	<title>Mantra Registration</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<link rel="stylesheet" type="text/css" href="{!! asset('css/roboto-font.css') !!}">
	<link rel="stylesheet" type="text/css" href="{!! asset('fonts/material-design-iconic-font/css/material-design-iconic-font.min.css') !!}">
	<!-- datepicker -->
	<link rel="stylesheet" type="text/css" href="{!! asset('css/jquery-ui.min.css') !!}">
	<!-- Main Style Css -->
    <link rel="stylesheet" type="text/css" href="{!! asset('css/style.css') !!}"/>
</head>
<body>
	<div class="page-content" style="background-image: url('images/wizard-v3.jpg')">
		<div class="wizard-v3-content">
			<div class="wizard-form">
				<div class="wizard-header">
					<h3 class="heading">Sign Up Your User Account</h3>
					<p>Fill all form field to go next step</p>
				</div>
		        <form role="form" name="regForm" id="regForm" class="form-register" action="{{ route('register') }}" method="post">
					{{ csrf_field() }}
					<div id="form-total">
		        		<!-- SECTION 1 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-account"></i></span>
			            	<span class="step-text">About</span>
			            </h2>
			            <section>
			                <div class="inner">
								<div name="s1Errors" id="s1Errors" style="display:none; color:red;">
									<h3>Errors:</h3>
									<div class="form-row">
										<ul name="_s1_Errors" id="_s1_Errors">
										</ul>
									</div>
								</div>
			                	<h3>Account Information:</h3>
			                	<div class="form-row">
									<div class="form-holder form-holder-2">
										<label class="form-row-inner">
											<input type="text" name="email" id="email" class="form-control" required>
											<span class="label">Email Address</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label class="form-row-inner">
											<input type="password" name="password_1" id="password_1" class="form-control" required>
											<span class="label">Password</span>
											<span class="border"></span>
										</label>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label class="form-row-inner">
											<input type="password" name="confirm_password_1" id="confirm_password_1" class="form-control" required>
											<span class="label">Confirm Password</span>
											<span class="border"></span>
										</label>
									</div>
								</div>
							</div>
			            </section>
						<!-- SECTION 2 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-lock"></i></span>
			            	<span class="step-text">Personal</span>
			            </h2>
			            <section>
			                <div class="inner">
								<div name="s2Errors" id="s2Errors" style="display:none; color:red;">
									<h3>Errors:</h3>
									<div class="form-row">
										<ul name="_s2_Errors" id="_s2_Errors">
										</ul>
									</div>
								</div>
			                	<h3>Personal Information:</h3>
								<div class="form-row">
									<div class="form-holder">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="nickname" name="nickname" required>
											<span class="label">Nickname*</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
			                	<div class="form-row">
									<div class="form-holder">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="first_name" name="first_name" required>
											<span class="label">First Name*</span>
					  						<span class="border"></span>
										</label>
									</div>
									<div class="form-holder">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="last_name" name="last_name" required>
											<span class="label">Last Name*</span>
					  						<span class="border"></span>
										</label>
									</div>
									<div class="form-holder">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="suffix" name="suffix" required>
											<span class="label">Suffix</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
								<div class="form-row">
									<div id="radio">
										<label>Gender*:</label>
										<input type="radio" name="gender" value="Male" checked class="radio-1"> Male
  										<input type="radio" name="gender" value="Female"> Female
									</div>
								</div>
								<div class="form-row form-row-date">
									<div class="form-holder form-holder-2">
										<label for="date" class="special-label">Date of Birth*:</label>
										<select name="date" id="date">
											<option value="Day" disabled selected>Day</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
										</select>
										<select name="month" id="month">
											<option value="Month" disabled selected>Month</option>
											<option value="Feb">Feb</option>
											<option value="Mar">Mar</option>
											<option value="Apr">Apr</option>
											<option value="May">May</option>
										</select>
										<select name="year" id="year">
											<option value="Year" disabled selected>Year</option>
											<option value="2017">2017</option>
											<option value="2016">2016</option>
											<option value="2015">2015</option>
											<option value="2014">2014</option>
											<option value="2013">2013</option>
										</select>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="telephone_number" name="telephone_number" required>
											<span class="label">Telephone Number*</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="mobile_number" name="mobile_number" required>
											<span class="label">Mobile Number*</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-1">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="nationality" name="nationality" required>
											<span class="label">Nationality*</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-1">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="country" name="country" required>
											<span class="label">Country*</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-1">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="address" name="address" required>
											<span class="label">Mailing Address*</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
							</div>
			            </section>
			            <!-- SECTION 3 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-card"></i></span>
			            	<span class="step-text">Company</span>
			            </h2>
			            <section>
			                <div class="inner">
								<div name="s3Errors" id="s3Errors" style="display:none; color:red;">
									<h3>Errors:</h3>
									<div class="form-row">
										<ul name="_s3_Errors" id="_s3_Errors">
										</ul>
									</div>
								</div>
			                	<h3>Company Information:</h3>
								<div class="form-row">
									<div class="form-holder form-holder-1">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="business_id" name="business_id" required>
											<span class="label">Business / Company ID (if joining)*</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-1">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="business_name" name="business_name" required>
											<span class="label">Company Name*</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-1">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="business_address" name="business_address" required>
											<span class="label">Business Address*</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-1">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="business_telephone" name="business_telephone" required>
											<span class="label">Business Telephone*</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-1">
										<label class="form-row-inner">
											<input type="text" class="form-control" id="business_tags" name="business_tags" required>
											<span class="label">Tags (something to identify you on searches)*</span>
					  						<span class="border"></span>
										</label>
									</div>
								</div>
							</div>
			            </section>
			            <!-- SECTION 4 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-receipt"></i></span>
			            	<span class="step-text">Confirm</span>
			            </h2>
			            <section>
			                <div class="inner">
			                	<h3>Confirm Details:</h3>
			                	<div class="form-row table-responsive">
									<table class="table">
										<tbody>
											<tr class="space-row">
												<th>Nickname:</th>
												<td id="nickname-val"></td>
											</tr>
											<tr class="space-row">
												<th>Full Name:</th>
												<td id="fullname-val"></td>
											</tr>
											<tr class="space-row">
												<th>Email Address:</th>
												<td id="email-val"></td>
											</tr>
											<tr class="space-row">
												<th>Telephone Number:</th>
												<td id="telephone-val"></td>
											</tr>
											<tr class="space-row">
												<th>Mobile Number:</th>
												<td id="mobile-val"></td>
											</tr>
											<tr class="space-row">
												<th>Gender:</th>
												<td id="gender-val"></td>
											</tr>
											<tr class="space-row">
												<th>Birthdate:</th>
												<td id="birthdate-val"></td>
											</tr>
											<tr class="space-row">
												<th>Nationality:</th>
												<td id="nationality-val"></td>
											</tr>
											<tr class="space-row">
												<th>Country:</th>
												<td id="country-val"></td>
											</tr>
											<tr class="space-row">
												<th>Address:</th>
												<td id="address-val"></td>
											</tr>
											<tr class="space-row">
												<th>Business ID:</th>
												<td id="business-id-val"></td>
											</tr>
											<tr class="space-row">
												<th>Business Name:</th>
												<td id="business-name-val"></td>
											</tr>
											<tr class="space-row">
												<th>Business Telephone Number:</th>
												<td id="business-tel-val"></td>
											</tr>
											<tr class="space-row">
												<th>Business Address:</th>
												<td id="business-address-val"></td>
											</tr>
											<tr class="space-row">
												<th>Business Tags:</th>
												<td id="business-tags-val"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
			            </section>
		        	</div>
		        </form>
			</div>
		</div>
	</div>
    <!-- Jquery JS-->
    <script src="{!! asset('vendor/jquery-3.2.1.min.js') !!}"></script>
	<script src="{!! asset('js/register/jquery.steps.js') !!}"></script>
	<script src="{!! asset('js/register/jquery-ui.min.js') !!}"></script>
	<script src="{!! asset('js/register/main.js') !!}"></script>
</body>
</html>