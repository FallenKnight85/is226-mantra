<?php
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('user_profiles')->delete();
        DB::table('company_profiles')->delete();

        $User = new \App\User();
        $User->UserID = Uuid::generate()->string;
        $User->Username = 'admin@mantra.is226.com';
        $User->password = Hash::make('mantradmin69');
        $User->email = 'admin@mantra.is226.com';
        $User->created_at = Carbon::now();
        $User->updated_at = Carbon::now();
        $User->save();

        $UserProfile = new \App\UserProfile();
        $UserProfile->ProfileID = Uuid::generate()->string;
        $UserProfile->UserID = $User->UserID;
        $UserProfile->CompanyID = 0;
        $UserProfile->LastName = 'Admin';
        $UserProfile->GivenName = 'Mantra';
        $UserProfile->Suffix = '';
        $UserProfile->NickName = 'Madmin';
        $UserProfile->MailingAddress = 'Somewhere out there';
        $UserProfile->EmailAddress = $User->email;
        $UserProfile->Birthdate = Carbon::parse('May 20, 1995');
        $UserProfile->Gender = 'Male';
        $UserProfile->TelephoneNumber = '000000123';
        $UserProfile->MobileNumber = '09778531733';
        $UserProfile->Nationality = 'Filipino';
        $UserProfile->Country = 'Philippines';
        $UserProfile->created_at = Carbon::now();
        $UserProfile->updated_at = Carbon::now();
        $UserProfile->save();

        $CompanyProfile = new \App\CompanyProfile();
        $CompanyProfile->CompanyID = Uuid::generate()->string;
        $CompanyProfile->OwnerID = 0;
        $CompanyProfile->CompanyName = "IS226 Project Mantra";
        $CompanyProfile->BusinessAddress = "UPOU";
        $CompanyProfile->BusinessTelephone = "01234555413";
        $CompanyProfile->CompanyTags = json_encode(['developer', 'siteowner', 'delivery']);
        $CompanyProfile->created_at = Carbon::now();
        $CompanyProfile->updated_at = Carbon::now();
        $CompanyProfile->save();

        $UserProfile->CompanyID = $CompanyProfile->CompanyID;
        $UserProfile->save();

        $CompanyProfile->OwnerID = $UserProfile->ProfileID;
        $CompanyProfile->save();
    }
}