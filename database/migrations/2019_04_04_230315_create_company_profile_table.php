<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('CompanyID')->unique();
            $table->uuid('OwnerID')->unique();
            $table->string('CompanyName')->unique();
            $table->longText('BusinessAddress')->nullable();
            $table->string('BusinessTelephone')->nullable();
            $table->longText('CompanyTags')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_profile');
    }
}
