<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('ProfileID')->unique();
            $table->uuid('UserID')->unique();
            $table->string('LastName')->nullable();
            $table->string('GivenName')->nullable();
            $table->string('Suffix')->nullable();
            $table->string('NickName')->nullable();
            $table->longText('MailingAddress')->nullable();
            $table->string('EmailAddress')->nullable();
            $table->date('BirthDate')->nullable();
            $table->enum('Gender',['Male', 'Female'])->nullable();
            $table->string('Telephone Number')->nullable();
            $table->string('Mobile Number')->nullabe();
            $table->string('Nationality')->nullable();
            $table->string('Country')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_profile');
    }
}
