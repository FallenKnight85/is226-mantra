<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUsersProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement('ALTER TABLE users_profile CHANGE `Telephone Number` TelephoneNumber VARCHAR(191)');
        DB::statement('ALTER TABLE users_profile CHANGE `Mobile Number` MobileNumber VARCHAR(191)');
        DB::statement('ALTER TABLE `users_profile` ADD COLUMN `CompanyID` char(36) NOT NULL AFTER `UserID`;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement('ALTER TABLE users_profile CHANGE TelephoneNumber `Telephone Number` VARCHAR(191)');
        DB::statement('ALTER TABLE users_profile CHANGE MobileNumber `Mobile Number` VARCHAR(191)');
        DB::statement('ALTER TABLE users_profile DROP CompanyID');
    }
}
