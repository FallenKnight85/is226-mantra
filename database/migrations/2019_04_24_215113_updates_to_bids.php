<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatesToBids extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bids', function (Blueprint $table) {
            //
            $table->dropColumn('BidTitle');
            $table->dropColumn('BidDescription');
            $table->longText('resource')->nullable()->after('CreatebyProfileID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bids', function (Blueprint $table) {
            //
            $table->string('BidTitle');
            $table->longText('BidDescription');
            $table->dropColumn('resource');
        });
    }
}
