<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBids extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bids', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('BidID')->unique();
            $table->uuid('ForCompanyID');
            $table->uuid('ByCompanyID');
            $table->uuid('ForProjectID');
            $table->uuid('CreatebyProfileID');
            $table->string('BidTitle');
            $table->longText('BidDescription');
            $table->double('BidCost');
            $table->string('BidStatus')->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bids');
    }
}
