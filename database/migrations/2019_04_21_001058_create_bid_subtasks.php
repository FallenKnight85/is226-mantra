<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidSubtasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bid_subtasks', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('BidSubTaskID')->unique();
            $table->uuid('BidTaskID');
            $table->uuid('CreatedbyProfileID');
            $table->longText('SubtaskDetails');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bid_subtasks');
    }
}
