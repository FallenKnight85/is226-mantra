<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bid_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('BidTaskID')->unique();
            $table->uuid('BidID');
            $table->uuid('CreatedbyProfileID');
            $table->string('TaskName');
            $table->string('TaskDescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bid_tasks');
    }
}
